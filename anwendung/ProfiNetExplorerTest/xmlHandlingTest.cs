﻿using System;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProfinetExplorer;
using Assert = NUnit.Framework.Assert;

namespace ProfiNetExplorerTest
{
    /// <summary>
    /// tests the ParseGsd function, which validates the gsdml files with the xsd scheme
    /// </summary>
    [TestClass]
    public class xmlHandlingTest
    {
        // tests loading a correct gsd file
        [TestMethod]
        public void parseGsdTrue()
        {
            var mainDialog = new MainDialog();
            var curDirPath = Environment.CurrentDirectory;
            var tempPath = curDirPath.Substring(0, curDirPath.Length - 11);
            var pathToGsd = tempPath + "\\gsdForTest\\GSDML-V2.33-Balluff-BNI-PNT-508-105-Z015-20180601.xml";
            var gsd =  mainDialog.parse_gsd(pathToGsd);
            Assert.IsNotNull(gsd);
        }

        // tests an incorrect gsd file
        [TestMethod]
        public void parseGsdFalse()
        {
            var mainDialog = new MainDialog();
            var curDirPath = Environment.CurrentDirectory;
            var tempPath = curDirPath.Substring(0, curDirPath.Length - 11);
            var pathToGsd = tempPath + "\\gsdForTest\\GSDML-V2.31-PNO-IO-Link-20170523.xml";
            try
            {
                var gsd = mainDialog.parse_gsd(pathToGsd);
            }
            catch(Exception e)
            {
                StringAssert.StartsWith(e.ToString(), "System.NullReferenceException: Object reference not set to an instance of an object.\r\n   at ProfinetExplorer.MainDialog.parse_gsd(String path) in");
            }
        }

    }
    }
