﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpPcap;

namespace ProfinetExplorer
{
    public class SharpPcapFactory: ISharpPcapFactory
    {
        public virtual CaptureDeviceList GetInstance()
        {
            return CaptureDeviceList.Instance;
        }
    }
}
