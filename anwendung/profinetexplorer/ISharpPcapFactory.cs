﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpPcap;

namespace ProfinetExplorer
{
    public interface ISharpPcapFactory
    {
        CaptureDeviceList GetInstance();
    }
}
