﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Profinet
{
    public class Vlan
    {
        public enum Priorities
        {
            /// <summary>
            /// DCP, IP
            /// </summary>
            Priority0 = 0,
            /// <summary>
            /// Low prior RTA_CLASS_1 or RTA_CLASS_UDP
            /// </summary>
            Priority5 = 5,
            /// <summary>
            /// RT_CLASS_UDP, RT_CLASS_1, RT_CLASS_2, RT_CLASS_3, high prior RTA_CLASS_1 or RTA_CLASS_UDP
            /// </summary>
            Priority6 = 6,
            /// <summary>
            /// PTCP-AnnouncePDU
            /// </summary>
            Priority7 = 7,
        }

        public enum Type : ushort
        {
            /// <summary>
            /// UDP, RPC, SNMP, ICMP
            /// </summary>
            Ip = 0x0800,
            Arp = 0x0806,
            TagControlInformation = 0x8100,
            /// <summary>
            /// RTC, RTA, DCP, PTCP, FRAG
            /// </summary>
            Pn = 0x8892,
            Ieee802_1As = 0x88F7,
            Lldp = 0x88CC,
            Mrp = 0x88E3,
        }

        public static int Encode(System.IO.Stream buffer, Priorities priority, Type type)
        {
            UInt16 tmp = 0;

            //Priority
            tmp |= (UInt16)((((UInt16)priority) & 0x7) << 13);

            //CanonicalFormatIdentificator
            tmp |= 0 << 12;

            //VLAN_Id
            tmp |= 0;

            Dcp.EncodeU16(buffer, tmp);
            Dcp.EncodeU16(buffer, (UInt16)type);

            return 4;
        }
    }
}
