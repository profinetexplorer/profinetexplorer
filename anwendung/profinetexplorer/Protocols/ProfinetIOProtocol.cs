﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;

namespace Profinet
{
    public class ProfinetIo
    {
        public enum BlockTypes : ushort
        {
            //0x0000 Reserved —
            AlarmNotificationHigh = 0x0001,         // RTA-SDU.AlarmNotification-PDU
            AlarmNotificationLow = 0x0002,          // RTA-SDU.AlarmNotification-PDU
            //0x0003 – 0x0007 Reserved —
            IodWriteReqHeader = 0x0008,             // PROFINETIO-ServiceReqPDU.IODWriteReq
            IodReadReqHeader = 0x0009,              // PROFINETIO-ServiceReqPDU.IODReadReq
            //0x000A – 0x000F Reserved —
            DiagnosisData = 0x0010,                 // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.DiagnosisData RTA-SDU.AlarmNotification-PDU.AlarmPayload.DiagnosisItem.DiagnosisData
            //0x0011 Reserved —
            ExpectedIdentificationData = 0x0012,    // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.ExpectedIdentificationData
            RealIdentificationData = 0x0013,        // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.RealIdentificationData
            SubstituteValue = 0x0014,               // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.SubstituteValue
            // PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.SubstituteValue
            RecordInputDataObjectElement = 0x0015,  // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.RecordInputDataObjectElement
            RecordOutputDataObjectElement = 0x0016, // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.RecordOutputDataObjectElement
            //0x0017 Reserved —
            ArData = 0x0018,                        // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.ARData
            LogBookData = 0x0019,                   // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.LogBookData
            ApiData = 0x001A,                       // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.APIData
            SrlData = 0x001B,                       // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.ARDataInfo.SRLData
            //0x001C – 0x001F Reserved —
            M0 = 0x0020,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M0– 350 – 61158–6–10/CD ED 4 © IEC (E):2015 Value (hexadecimal) Used for Used by
            M1 = 0x0021,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M1 PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.IMDataWrite.I&M1
            M2 = 0x0022,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M2 PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.IMDataWrite.I&M2
            M3 = 0x0023,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M3 PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.IMDataWrite.I&M3
            M4 = 0x0024,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M4 PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.IMDataWrite.I&M4
            M5 = 0x0025,                          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M5 
            M6Start = 0x0026,
            M6End = 0x002F,                      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite
            M0FilterDataSubmodule = 0x0030,       // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.I&M0FilterData.I&M0FilterDataSubmodule
            M0FilterDataModule = 0x0031,          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.I&M0FilterData.I&M0FilterDataModule
            M0FilterDataDevice = 0x0032,          // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.I&M0FilterData.I&M0FilterDataDevice
            //0x0033 Reserved —
            M5Data = 0x0034,                      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IMData.I&M5.I&M5Data
            //0x0035 – 0x0100 Reserved —
            ArBlockReq = 0x0101,                    // PROFINETIOServiceReqPDU.IODConnectReq.ARBlockReq
            IocrBlockReq = 0x0102,                  // PROFINETIOServiceReqPDU.IODConnectReq.IOCRBlockReq
            AlarmCrBlockReq = 0x0103,               // PROFINETIOServiceReqPDU.IODConnectReq.AlarmCRBlockReq
            ExpectedSubmoduleBlockReq = 0x0104,     // PROFINETIO-ServiceReqPDU.IODConnectReq.ExpectedSubmoduleBlockReq
            PrmServerBlockReq = 0x0105,             // PROFINETIOServiceReqPDU.IODConnectReq.PrmServerBlockReq 
            McrBlockReq = 0x0106,                   // PROFINETIOServiceReqPDU.IODConnectReq.MCRBlockReq
            ArrpcBlockReq = 0x0107,                 // PROFINETIOServiceReqPDU.IODConnectReq.ARRPCBlockReq
            ArVendorBlockReq = 0x0108,              // PROFINETIOServiceReqPDU.IODConnectReq.ARVendorBlockReq
            IrInfoBlock = 0x0109,                   // PROFINETIOServiceReqPDU.IODConnectReq.IRInfoBlock
            SrInfoBlock = 0x010A,                   // PROFINETIOServiceReqPDU.IODConnectReq.SRInfoBlock
            ArfsuBlock = 0x010B,                    // PROFINETIOServiceReqPDU.IODConnectReq.ARFSUBlock
            //0x010C – 0x010F Reserved —
            IodBlockReqControlBlockConnectPrmEnd = 0x0110,// shall only be used in conjunction with connection establishment phase PROFINETIOServiceReqPDU.IODControlReq.ControlBlockConnect (PrmEnd.req)
            IodBlockReqControlBlockPlugPrmEnd = 0x0111,   // shall only be used in conjunction with a plug alarm event PROFINETIOServiceReqPDU.IODControlReq.ControlBlockPlug (PrmEnd.req)
            IoxBlockReqControlBlockConnectApplicationReady = 0x0112,// shall be used in conjunction within the connection establishment phase or a PrmBegin/PrmEnd sequence PROFINETIOServiceReqPDU.IOXControlReq.ControlBlockConnect (ApplicationReady.req)
            IoxBlockReqControlBlockPlugApplicationReady = 0x0113,  // shall only be used in conjunction with a plug alarm event PROFINETIOServiceReqPDU.IOXControlReq.ControlBlockPlug (ApplicationReady.req)
            ReleaseBlockReq = 0x0114,               // PROFINETIOServiceReqPDU.IODReleaseReq.ReleaseBlock
            //0x0115 Reserved —
            IoxBlockReqControlBlockConnectReadyForCompanion = 0x0116,// shall only be used in conjunction with connection establishment phase PROFINETIOServiceReqPDU.IOXControlReq.ControlBlockConnect (ReadyForCompanion.req)
            IoxBlockReqControlBlockConnectReadyForRtClass3 = 0x0117,// shall only be used in conjunction with connection establishment phase PROFINETIOServiceReqPDU.IOXControlReq.ControlBlockConnect (ReadyForRT_CLASS_3.req)
            IodBlockReqControlBlockConnectPrmBegin = 0x0118,// PROFINETIOServiceReqPDU.IODControlReq.ControlBlockConnect (PrmBegin.req)
            SubmoduleListBlock = 0x0119,            // PROFINETIOServiceReqPDU.IODControlReq.SubmoduleListBlock (PrmBegin.req)
            //0x0118 – 0x01FF Reserved —
            PdPortDataCheck = 0x0200,               // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDPortDataCheck
            PdevData = 0x0201,                      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PdevData
            PdPortDataAdjust = 0x0202,              // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDPortDataAdjust
            PdSyncData = 0x0203,                    // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDSyncData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDSyncData
            IsochronousModeData = 0x0204,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.IsochronousModeData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.IsochronousModeData
            PdirData = 0x0205,                      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRData
            PdirGlobalData = 0x0206,                // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRGlobalData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRGlobalData
            PdirFrameData = 0x0207,                 // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRFrameData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRFrameData
            PdirBeginEndData = 0x0208,              // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRBeginEndData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRBeginEndData
            AdjustDomainBoundary = 0x0209,          // Sub block for adjusting DomainBoundary PROFINETIO-ServiceReqPDU.IODReadReq.RecordDataRead. PDPortDataAdjust.AdjustDomainBoundary PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustDomainBoundary
            //0x020A Sub block for checking Peers PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckPeers PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckPeers
            //0x020B Sub block for checking LineDelay PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckLineDelay PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckLineDelay
            //0x020C Sub block for checking MAUType PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckMAUType PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckMAUType
            AdjustMauType = 0x020E,                 // Sub block for adjusting MAUType PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustMAUType PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustMAUType
            PdPortDataReal = 0x020F,                // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortDataReal
            AdjustMulticastBoundary = 0x0210,       // Sub block for adjusting MulticastBoundary PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustMulticastBoundary PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustMulticastBoundary 
            PdInterfaceMrpDataAdjust = 0x0211,      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDInterfaceMrpDataAdjust
            PdInterfaceMrpDataReal = 0x0212,        // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDInterfaceMrpDataReal 
            PdInterfaceMrpDataCheck = 0x0213,       // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataCheck PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataCheck PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDInterfaceMrpDataCheck
            PdPortMrpDataAdjust = 0x0214,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortMrpDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortMrpDataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDPortMrpDataAdjust
            PdPortMrpDataReal = 0x0215,             // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortMrpDataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortMrpDataReal 
            MrpManagerParams = 0x0216,              // Sub block for media redundancy manager parameters PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataAdjust.MrpManagerParams PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataAdjust.MrpManagerParams PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataReal.MrpManagerParams
            MrpClientParams = 0x0217,               // Sub block for media redundancy client parameters PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataAdjust.MrpClientParams PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataAdjust.MrpClientParams PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataReal.MrpClientParams
            MrpRingStateData = 0x0219,              // Sub block for media redundancy ring state data PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataReal.MrpRingStateData 
            AdjustLinkState = 0x021B,               // Sub block for adjusting LinkState PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustLinkState PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustLinkState
            CheckLinkState = 0x021C,                // Sub block for checking LinkState PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckLinkState PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.CheckLinkState
            CheckSyncDifference = 0x021E,           // Sub block for checking local and remote CableDelay detected by LLDP to discover a sync difference PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckSyncDifference PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckSyncDifference
            CheckMauTypeDifference = 0x021F,        // Sub block for checking local and remote MAUTypes detected by LLDP PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck.CheckMAUTypeDifference PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckMAUTypeDifference
            PdPortFoDataReal = 0x0220,              // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortFODataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortFODataReal 
            FiberOpticManufacturerSpecific = 0x0221,// Sub block for reading real fiber optic manufacturerspecific data PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortFODataReal.FiberOpticManufacturerSpecific PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortFODataReal.FiberOpticManufacturerSpecific
            PdPortFoDataAdjust = 0x0222,            // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortFODataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortFODataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDPortFODataAdjust
            PdPortFoDataCheck = 0x0223,             // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortFODataCheck PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortFODataCheck PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDPortFODataCheck
            AdjustPeerToPeerBoundary = 0x0224,      // Sub block for adjusting the peer to peer boundary PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustPeerToPeerBoundary PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustPeerToPeerBoundary
            AdjustDcpBoundary = 0x0225,             // Sub block for adjusting the DCP boundary PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustDCPBoundary PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustDCPBoundary
            AdjustPreambleLength = 0x0226,          // Sub block for adjusting the used length of preamble PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustPreambleLength PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustPreambleLength
            CheckMauTypeExtension = 0x0227,         // Sub block for checking MAUTypeExtension PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataCheck. CheckMAUTypeExtension PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataCheck.CheckMAUTypeExtension
            FiberOpticDiagnosisInfo = 0x0228,       // Sub block for reading real fiber optic diagnosis data PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortFODataReal.FiberOpticDiagnosisInfo PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.FiberOpticDiagnosisInfo
            AdjustMauTypeExtension = 0x0229,        // Sub block for adjusting MAUTypeExtension PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataAdjust.AdjustMAUTypeExtension PROFINETIO-ServiceReqPDU.IODWriteReq.RecordDataWrite.PDPortDataAdjust.AdjustMAUTypeExtension
            PdirSubframeData = 0x022A,              // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRSubframeData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRSubframeData PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PdevData.PDIRSubframeData 
            SubframeBlock = 0x022B,                 // Sub block for the persistent portion of DFP PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDIRSubframeData.SubframeBlock PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDIRSubframeData.SubframeBlock PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PdevData.PDIRSubframeData.SubframeBlock
            PdPortDataRealExtended = 0x022C,        // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataRealExtended PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortDataRealExtended 
            PdTimeData = 0x022D,                    // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDTimeData PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDTimeData
            //0x022E – 0x22F Reserved
            PdncDataCheck = 0x0230,                 // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDNCDataCheck PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDNCDataCheck PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDNCDataCheck
            MrpInstanceDataAdjustBlock = 0x0231,    // Sub block of PDInterfaceMrpDataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataAdjust
            MrpInstanceDataRealBlock = 0x0232,      // Sub block of PDInterfaceMrpDataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataReal PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataReal
            MrpInstanceDataCheckBlock = 0x0233,     // Sub block of PDInterfaceMrpDataCheck PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceMrpDataCheck PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceMrpDataCheck
            //0x0234 – 0x23F Reserved —
            PdInterfaceDataReal = 0x0240,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceDataReal PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDInterfaceDataReal
            //0x0241 – 0x024F Reserved —
            PdInterfaceAdjust = 0x0250,             // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDInterfaceAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDInterfaceAdjust
            PdPortStatistic = 0x0251,               // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortStatistic PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortStatistic
            //0x0252 – 0x025F Reserved
            OwnPort = 0x0260,                       // Sub block of PDPortDataRealExtended PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataReal.OwnPort PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortDataRealExtended.OwnPort
            Neighbors = 0x0261,                     // Sub block of PDPortDataRealExtended PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDPortDataRealExtended.Neighbors PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.PDPortDataRealExtended.Neighbors
            //0x0262 – 0x03FF Reserved —
            MultipleBlockHeader = 0x0400,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDRealData.MultipleBlockHeader PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.MultipleBlockHeader
            CoContainerContent = 0x0401,            // PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.CombinedObjectContainer.COContainerContent
            //0x0402 – 0x04FF Reserved —
            RecordDataReadQuery = 0x0500,           // PROFINETIOServiceResPDU.IODReadReq.RecordDataReadQuery
            //0x0501 – 0x05FF Reserved —
            FsHelloBlock = 0x0600,                  // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceFSUDataAdjust.FSHelloBlock PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceFSUDataAdjust.FSHelloBlock PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDInterfaceFSUDataAdjust.FSHelloBlock
            FsParameterBlock = 0x0601,              // PROFINETIOServiceReqPDU.IODConnectReq.IOCRBlockReq.ARFSUBlock.ARFSUDataAdjust.FSParameterBlock
            //0x0602 – 0x607 Reserved for FastStartUp —
            PdInterfaceFsuDataAdjust = 0x0608,      // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDInterfaceFSUDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.PDInterfaceFSUDataAdjust PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PDExpectedData.PDInterfaceFSUDataAdjust
            ArfsuDataAdjust = 0x0609,               // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.ARFSUDataAdjust PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite.ARFSUDataAdjust PROFINETIOServiceReqPDU.IODConnectReq.ARFSUBlock.ARFSUDataAdjust
            //0x060A – 0x60F Reserved for FastStartUp —
            //0x0610 – 0x06FF Reserved —
            AutoConfiguration = 0x0700,             // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.AutoConfiguration
            AutoConfigurationCommunication = 0x0701,// PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.AutoConfiguration.ACCommunication
            AutoConfigurationConfiguration = 0x0702,// PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.AutoConfiguration.ACConfiguration
            AutoConfigurationIsochronous = 0x0703,  // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.AutoConfiguration.ACIsochronous
            //0x0704 – 0x07FF Reserved —
            //0x0800 Reserved for profiles covering energy saving BlockType for request service PROFINETIOServiceReqPDU.IODWriteReq.RecordDataWrite
            //0x0801 Reserved for profiles covering energy saving BlockType for response service PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead
            //0x0802 – 0x080F Reserved for profiles covering energy saving —
            PeEntityFilterData = 0x0810,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PE_EntityFilterData
            PeEntityStatusData = 0x0811,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.PE_EntityStatusData
            //0x0812 – 0x08FF Reserved for profiles covering energy saving —
            //0x0900 – 0x09FF Reserved for profiles covering sequence of events —
            UploadBlobQuery = 0x0A00,               // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.UploadBLOBQuery
            UploadBlob = 0x0A01,                    // PROFINETIOServiceResPDU.IODReadRes.RecordDataRead.UploadBLOB
            NestedDiagnosisInfo = 0x0A02,           // PROFINETIOServiceReqPDU.IODReadReq.RecordDataRead.NestedDiagnosisInfo
            //0x0A03 – 0x0AFF Reserved for profiles covering controller to controller communication —
            //0x0B00 – 0x0EFF Reserved 
            MaintenanceItem = 0x0F00,               // RTA-SDU.AlarmnotificationPDU.AlarmPayload.MaintenanceItem
            UploadSelectedRecordsWithinUploadRetrievalItem = 0x0F01,   // RTA-SDU.AlarmnotificationPDU.AlarmPayload.Upload&RetrievalItem
            IParameterItem = 0x0F02,                // RTA-SDU.AlarmnotificationPDU.AlarmPayload.iParameterItem
            RetrieveSelectedRecordsWithinUploadRetrievalItem = 0x0F03, // RTA-SDU.AlarmnotificationPDU.AlarmPayload.Upload&RetrievalItem
            RetrieveAllStoredRecordsWithinUploadRetrievalItem = 0x0F04,    // RTA-SDU.AlarmnotificationPDU.AlarmPayload.Upload&RetrievalItem
            SignalApeOperationalModeChangeWithinPeEnergySavingStatus = 0x0F05,    // RTA-SDU.AlarmnotificationPDU.AlarmPayload.PE_AlarmItem
            //0x0F06 – 0x6FFF Reserved —
            //0x7000 – 0x7FFF Reserved for vendor specific data objects —
            //0x8000 Reserved —
            AlarmAckHigh = 0x8001,                  // RTA-SDU.AlarmAck-PDU
            AlarmAckLow = 0x8002,                   // RTA-SDU.AlarmAck-PDU
            IodWriteResHeader = 0x8008,             // PROFINETIO-ServiceResPDU.IODWriteRes
            IodReadResHeader = 0x8009,              // PROFINETIO-ServiceReqPDU.IODReadRes
            //0x800A – 0x8100 Reserved —
            ArBlockRes = 0x8101,                    // PROFINETIOServiceResPDU.IODConnectRes.ARBlockRes
            IocrBlockRes = 0x8102,                  // PROFINETIOServiceResPDU.IODConnectRes.IOCRBlockRes
            AlarmCrBlockRes = 0x8103,               // PROFINETIOServiceResPDU.IODConnectRes.AlarmCRBlockRes
            ModuleDiffBlock = 0x8104,               // PROFINETIOServiceResPDU.IODConnectRes.ModuleDiffBlock PROFINETIOServiceResPDU.IODWriteRes.RecordDataRead.ModuleDiffBlock PROFINETIOServiceReqPDU.IOXControlReq.ModuleDiffBlock
            PrmServerBlockRes = 0x8105,             // PROFINETIOServiceResPDU.IODConnectRes.PrmServerBlockRes
            ArServerBlockRes = 0x8106,              // PROFINETIOServiceResPDU.IODConnectRes.ARServerBlockRes
            ArrpcBlockRes = 0x8107,                 // PROFINETIOServiceResPDU.IODConnectRes.ARRPCBlockRes
            ArVendorBlockRes = 0x8108,              // PROFINETIOServiceResPDU.IODConnectRes.ARVendorBlockRes
            //0x8109 – 0x810F Reserved —
            IodBlockResControlBlockConnectPrmEnd = 0x8110, // shall only be used in conjunction with connection establishment phase PROFINETIOServiceResPDU.IODControlRes.ControlBlockConnect (PrmEnd.rsp)
            IodBlockResControlBlockPlugPrmEnd = 0x8111, // shall only be used in conjunction with a plug alarm event PROFINETIOServiceResPDU.IODControlRes.ControlBlockPlug (PrmEnd.rsp)
            IoxBlockResControlBlockConnectApplicationReady = 0x8112, // shall only be used in conjunction with connection establishment phase PROFINETIOServiceResPDU.IOXControlRes.ControlBlockConnect (ApplRdy.rsp)
            IoxBlockResControlBlockPlugApplicationReady = 0x8113, // shall only be used in conjunction with a plug alarm event PROFINETIOServiceResPDU.IOXControlRes.ControlBlockPlug (ApplRdy.rsp)
            ReleaseBlockRes = 0x8114,               // PROFINETIOServiceResPDU.IODReleaseRes.ReleaseBlock
            //0x8115 Reserved —
            IoxBlockResControlBlockConnectReadyForCompanion = 0x8116,  // shall only be used in conjunction with connection establishment phase PROFINETIOServiceResPDU.IOXControlRes.ControlBlockConnect (ReadyForCompanion.rsp)
            IoxBlockResControlBlockConnectReadyForRtClass3 = 0x8117, // shall only be used in conjunction with connection establishment phase PROFINETIOServiceResPDU.IOXControlRes.ControlBlockConnect (ReadyForRT_CLASS_3.rsp)
            IodBlockResControlBlockConnectPrmBegin = 0x8118,      // PROFINETIOServiceResPDU.IODControlRes.ControlBlockConnect (PrmBegin.rsp)
            //Other Reserved —
        }

        public enum ArTypes : ushort
        {
            //0x0000 Reserved —
            IocarSingle = 0x0001,
            //0x0002 – 0x0005 Reserved —
            Iosar = 0x0006, //The supervisor AR is a special form of the IOCARSingle allowing takeover of the ownership of a submodule
            //0x0007 – 0x000F Reserved —
            IocarSingleRtClass3 = 0x0010, // using RT_CLASS_3 This is a special form of the IOCARSingle indicating RT_CLASS_3 communication
            //0x0011 – 0x001F Reserved —
            Iocarsr = 0x0020,   //The SR AR is a special form of the IOCARSingle indicating system redundancy or dynamic reconfiguration usage
            //0x0021 – 0xFFFF Reserved
        }

        [Flags]
        public enum ArProperties : uint
        {
            StateActive = 1,
            SupervisorTakeoverNotAllowed = 0 << 3,
            SupervisorTakeoverAllowed = 1 << 3,
            ParameterizationServerExternalPrmServer = 0 << 4,
            ParameterizationServerCmInitiator = 1 << 4,
            DeviceAccessOnlySubmodulesFromExpectedSubmoduleBlockAccessible = 0 << 8,
            DeviceAccessSubmoduleAccessControlledIoDevice = 1 << 8,
            CompanionArSingle = 0 << 9,
            CompanionArFirst = 1 << 9,
            CompanionArCompanion = 2 << 9,
            AcknowledgeCompanionArNoRequired = 0 << 11,
            AcknowledgeCompanionArWithAcknowledge = 1 << 11,
            StartupModeLegacy = 0 << 30,
            StartupModeAdvanced = 1 << 30,
            PullModuleAlarmAllowedMode0 = 0 << 31,
            PullModuleAlarmAllowedMode1 = 1u << 31,
        }

        public enum IocrTypes : ushort
        {
            //0x0000 Reserved
            Input = 0x0001,
            Output = 0x0002,
            MulticastProvider = 0x0003,
            MulticastConsumer = 0x0004,
            //0x0005 – 0xFFFF Reserved
        }

        public enum IocrProperties : uint
        {
            //0x00 Reserved —
            RtClass1 = 0x01,   // for the IOCRs Data-RTC-PDU IOCRProperties.RTClass == 0x02 shall be used as substitution by the engineering tool. Shall be supported for legacy IO devices by IO controller and IO supervisor. It should not be generated by an IO device.
            RtClass2 = 0x02,   // for the IOCRs Data-RTC-PDU
            RtClass3 = 0x03,   // for the IOCRs Data-RTC-PDU
            RtClass4 = 0x04, // for the IOCRs UDP-RTC-PDU
            //0x05 – 0x07 Reserved
        }

        [Flags]
        public enum IocrTagHeaders : ushort
        {
            IocrvlanidNoVlan = 0,
            IocrvlanidStart = 1,   //According IEEE 802.1Q
            IocrvlanidEnd = 0xFFF,
            IoUserPriorityCr = 0xC000,
        }

        [Flags]
        public enum AlarmCrProperties : uint
        {
            UserPriority = 0,
            FixedPriority = 1,
            RtaClass1 = 0,
            RtaClassUdp = 2,
        }

        public enum SubmoduleProperties : ushort
        {
            TypeNoIo = 0,
            TypeInput = 1,
            TypeOutput = 2,
            TypeInputOutput = 3,
            SharedInputIoControllerOnly = 0 << 2,
            SharedInputIoControllerShared = 1 << 2,
            ReduceInputSubmoduleDataLengthExpected = 0 << 3,
            ReduceInputSubmoduleDataLengthZero = 1 << 3,
            ReduceOutputSubmoduleDataLengthExpected = 0 << 4,
            ReduceOutputSubmoduleDataLengthZero = 1 << 4,
            DiscardIoxsExpected = 0 << 5,
            DiscardIoxsZero = 1 << 5,
        }

        public enum Index : ushort
        {
            /* user specific */
            //0 – 0x7FFF User specific RecordData

            /* subslot specific */
            ExpectedIdentificationDataSubslot = 0x8000, //for one subslot //All Needed
            RealIdentificationDataSubslot = 0x8001, //for one subslot //All —
            //0x8002 – 0x8009 Reserved — —
            DiagnosisChannelSubslot = 0x800A, //for one subslot //All Filter
            DiagnosisAllSubslot = 0x800B, //for one subslot //All Filter
            DiagnosisMaintenanceQualifiedStatusSubslot = 0x800C, //for one subslot //All Filter
            //0x800D – 0x800F Reserved — —
            MaintenanceRequiredChannelSubslot = 0x8010, //for one subslot //All Filter
            MaintenanceDemandedChannelSubslot = 0x8011, //for one subslot //All Filter
            MaintenanceRequiredAllSubslot = 0x8012, //for one subslot //All Filter
            MaintenanceDemandedAllSubslot = 0x8013, //for one subslot //All Filter
            //0x8014 – 0x801D Reserved — —
            SubstituteValue = 0x801E, //for one subslot //All —
            //0x801F Reserved — —
            PdirSubframeData = 0x8020, //for one subslot //Interface —
            //0x8021 – 0x8026 Reserved — —
            PdPortDataRealExtended = 0x8027, //for one subslot //Port —
            RecordInputDataObjectElement = 0x8028, //for one subslot //All —
            RecordOutputDataObjectElement = 0x8029, //for one subslot //All —
            PdPortDataReal = 0x802A, //for one subslot //Port —
            PdPortDataCheck = 0x802B, //for one subslot //Port —
            PdirData = 0x802C, //for one subslot //Interface —
            PdSyncData = 0x802D, //for one subslot with SyncID value 0 //Interface —
            //0x802E Reserved (legacy) — —
            PdPortDataAdjust = 0x802F, //for one subslot //Port —
            IsochronousModeData = 0x8030, //for one subslot //All —
            PdTimeData = 0x8031, //for one subslot //Interface —
            //0x8032 – 0x804F Reserved — —
            PdInterfaceMrpDataReal = 0x8050, //for one subslot //Interface —
            PdInterfaceMrpDataCheck = 0x8051, //for one subslot //Interface —
            PdInterfaceMrpDataAdjust = 0x8052, //for one subslot //Interface —
            PdPortMrpDataAdjust = 0x8053, //for one subslot //Port —
            PdPortMrpDataReal = 0x8054, //for one subslot //Port —
            //0x8055 – 0x805F Reserved — —
            PdPortFoDataReal = 0x8060, //for one subslot //Port —
            PdPortFoDataCheck = 0x8061, //for one subslot //Port —
            PdPortFoDataAdjust = 0x8062, //for one subslot //Port —
            //0x8063 – 0x806F Reserved — —
            PdncDataCheck = 0x8070, //for one subslot //Interface —
            PdInterfaceAdjust = 0x8071, //for one subslot //Interface —
            PdPortStatistic = 0x8072, //for one subslot //Interface, Port —
            //0x8073 – 0x807F Reserved — —
            PdInterfaceDataReal = 0x8080, //for one subslot //Interface —
            //0x8081 – 0x808F Reserved — —
            PdInterfaceFsuDataAdjust = 0x8090, //Interface —
            //0x8091 – 0x809F Reserved — —
            ProfilesCoveringEnergySavingRecord0 = 0x80A0, //All —
            //0x80A1 – 0x80AE Reserved //for profiles covering energy saving — —
            PeStatusData = 0x80AF, //for one subslot //All —
            CombinedObjectContainer = 0x80B0, //All —
            //0x80B1 – 0x80BF Reserved — —
            ProfilesCoveringSequenceOfEventsRecord0 = 0x80C0, //All —
            //0x80C1 – 0x80CF Reserved for profiles covering sequence of events — —
            //0x80D0 – 0xAFEF Reserved — —
            M0 = 0xAFF0, //All —
            M1 = 0xAFF1, //All —
            M2 = 0xAFF2, //All —
            M3 = 0xAFF3, //All —
            M4 = 0xAFF4, //All —
            M5 = 0xAFF5, //All —
            //0xAFF6 – 0xAFFF I&M6 – I&M15 Reserved for additional identification and maintenance data 
            //0xB000 – 0xBFFF Reserved for profiles

            /* slot specific */
            ExpectedIdentificationDataSlot = 0xC000, //for one slot //Needed
            RealIdentificationDataSlot = 0xC001, //for one slot —
            //0xC002 – 0xC009 Reserved —
            DiagnosisChannelSlot = 0xC00A, //for one slot //Filter
            DiagnosisAllSlot = 0xC00B, //for one slot //Filter
            DiagnosisMaintenanceQualifiedStatusSlot = 0xC00C, //for one slot //Filter
            //0xC00D – 0xC00F Reserved —
            MaintenanceRequiredChannelSlot = 0xC010, //for one slot //Filter
            MaintenanceDemandedChannelSlot = 0xC011, //for one slot //Filter
            MaintenanceRequiredAllSlot = 0xC012, //for one slot //Filter
            MaintenanceDemandedAllSlot = 0xC013, //for one slot //Filter
            //0xC014 – 0xCFFF Reserved —
            //0xD000 – 0xDFFF Reserved for profiles

            /* AR specific */
            ExpectedIdentificationDataAr = 0xE000, //for one AR Needed
            RealIdentificationDataAr = 0xE001, //for one AR Needed
            ModuleDiffBlock = 0xE002, //for one AR Needed
            //0xE003 – 0xE009 Reserved —
            DiagnosisChannelAr = 0xE00A, //for one AR Needed
            DiagnosisAllAr = 0xE00B, //for one AR Needed
            DiagnosisMaintenanceQualifiedStatusAr = 0xE00C, //for one AR Needed
            //0xE00D – 0xE00F Reserved —
            MaintenanceRequiredChannelAr = 0xE010, //for one AR Needed
            MaintenanceDemandedChannelAr = 0xE011, //for one AR Needed
            MaintenanceRequiredAllAr = 0xE012, //for one AR Needed
            MaintenanceDemandedAllAr = 0xE013, //for one AR Needed
            //0xE014 – 0xE02F Reserved —
            PeEntityFilterDataAr = 0xE030, //for one AR Needed
            PeEntityStatusDataAr = 0xE031, //for one AR Needed
            //0xE032 – 0xE03F Reserved —
            WriteMultiple = 0xE040,
            //0xE041 – 0xE04F Reserved —
            ArfsuDataAdjust = 0xE050, //for one AR Legacy, used only for ARProperties.StartupMode == Legacy Needed
            //0xE051 – 0xE05F Reserved for FastStartUp —
            //0xE060 – 0xEBFF Reserved —
            //0xEC00 – 0xEFFF Reserved for profiles Needed

            /* API specific */
            RealIdentificationDataApi = 0xF000, //for one API —
            //0xF001 – 0xF009 Reserved —
            DiagnosisChannelApi = 0xF00A, //for one API —
            DiagnosisAllApi = 0xF00B, //for one API —
            DiagnosisMaintenanceQualifiedStatusApi = 0xF00C, //for one API —
            //0xF00D – 0xF00F Reserved —
            MaintenanceRequiredChannelApi = 0xF010, //for one API —
            MaintenanceDemandedChannelApi = 0xF011, //for one API —
            MaintenanceRequiredAllApi = 0xF012, //for one API —
            MaintenanceDemandedAllApi = 0xF013, //for one API —
            //0xF014 – 0xF01F Reserved —
            ArDataApi = 0xF020, //for one API —
            //0xF021 – 0xF3FF Reserved —
            //0xF400 – 0xF7FF Reserved for profiles

            /* device specific */
            //0xF800 – 0xF80B Reserved —
            DiagnosisMaintenanceQualifiedStatusDevice = 0xF80C,//for one device —
            //0xF80D – 0xF81F Reserved —
            ArDataDevice = 0xF820,
            ApiData = 0xF821,
            //0xF822 – 0xF82F Reserved —
            LogBookData = 0xF830,
            PdevData = 0xF831,
            //0xF832 – 0xF83F Reserved —
            M0FilterData = 0xF840,
            PdRealData = 0xF841,
            PdExpectedData = 0xF842,
            //0xF843 – 0xF84F Reserved —
            AutoConfiguration = 0xF850,
            //0xF851 – F85F Reserved —
            GsdUpload = 0xF860,
            NestedDiagnosisInfo = 0xF861,
            //0xF862 – 0xF86F Reserved for controller to controller communication —
            PeEntityFilterDataDevice = 0xF870,
            PeEntityStatusDataDevice = 0xF871,
            //0xF872 – 0xFBFE Reserved —
            TriggerIndex = 0xFBFF,
            //0xFC00 – 0xFFFF Reserved for profiles a
        }

        public enum ControlCommands : ushort
        {
            PrmEnd = 1,
            ApplicationReady = 2,
            Release = 4,
            Done = 8,
            ReadyForCompanion = 16,
            ReadyForRtClass3 = 32,
            PrmBegin = 64,
        }

        public enum ControlBlockProperties : ushort
        {
            ImplicitControlCommandReadyForCompanion = 1,
            ImplicitControlCommandReadyForRtClass3 = 2,
        }

        private static int EncodeBlockHeader(System.IO.Stream buffer, BlockTypes type, UInt16 blockLength, out long blockLengthPosition)
        {
            int ret = 0;

            ret += Dcp.EncodeU16(buffer, (ushort)type);
            blockLengthPosition = buffer.Position;  //save it for later re-encode
            ret += Dcp.EncodeU16(buffer, blockLength);
            ret += Dcp.EncodeU16(buffer, 0x100);    //version

            return ret;
        }

        private static int DecodeBlockHeader(System.IO.Stream buffer, out BlockTypes type, out UInt16 blockLength)
        {
            int ret = 0;
            ushort tmp;

            ret += Dcp.DecodeU16(buffer, out tmp);
            type = (BlockTypes)tmp;
            ret += Dcp.DecodeU16(buffer, out blockLength);
            ret += Dcp.DecodeU16(buffer, out tmp);    //version

            return ret;
        }

        private static void ReEncodeBlockLength(System.IO.Stream buffer, long blockLengthPosition)
        {
            long currentPos = buffer.Position;
            buffer.Position = blockLengthPosition;
            Dcp.EncodeU16(buffer, (ushort)((currentPos - buffer.Position)-2));
            buffer.Position = currentPos;
        }

        public class ArBlockRequest : IProfinetSerialize
        {
            public ArTypes Type;
            public Guid Uuid;
            public UInt16 SessionKey;
            public System.Net.NetworkInformation.PhysicalAddress CmInitiatorMac;
            public Guid CmInitiatorObjectUuid;
            public ArProperties Properties;
            public UInt16 CmInitiatorActivityTimeoutFactor;
            public UInt16 CmInitiatorUdprtPort;
            public string CmInitiatorStationName;

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                ret += EncodeBlockHeader(buffer, BlockTypes.ArBlockReq, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, (ushort)Type);
                ret += Rpc.EncodeGuid(buffer, Rpc.Encodings.BigEndian, Uuid);
                ret += Dcp.EncodeU16(buffer, SessionKey);
                ret += Dcp.EncodeOctets(buffer, CmInitiatorMac.GetAddressBytes());
                ret += Rpc.EncodeGuid(buffer, Rpc.Encodings.BigEndian, CmInitiatorObjectUuid);
                ret += Dcp.EncodeU32(buffer, (uint)Properties);
                ret += Dcp.EncodeU16(buffer, CmInitiatorActivityTimeoutFactor);
                ret += Dcp.EncodeU16(buffer, CmInitiatorUdprtPort);
                ret += Dcp.EncodeU16(buffer, (ushort)CmInitiatorStationName.Length);
                ret += Dcp.EncodeString(buffer, CmInitiatorStationName);
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }
        }

        public class IocDataObject : IProfinetSerialize
        {
            public UInt16 SlotNumber { get; set; }
            public UInt16 SubslotNumber { get; set; }
            public UInt16 FrameOffset { get; set; }

            public IocDataObject(UInt16 slotNumber, UInt16 subslotNumber, UInt16 frameOffset)
            {
                SlotNumber = slotNumber;
                SubslotNumber = subslotNumber;
                FrameOffset = frameOffset;
            }

            // empty constructor required to build the project
            public IocDataObject()
            {
            }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += Dcp.EncodeU16(buffer, SlotNumber);
                ret += Dcp.EncodeU16(buffer, SubslotNumber);
                ret += Dcp.EncodeU16(buffer, FrameOffset);
                return ret;
            }
        }

        public class IocApi : IProfinetSerialize
        {
            public UInt32 No { get; set; }
            public IocDataObject[] DataObjects { get; set; }
            public IocDataObject[] Iocs { get; set; }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += Dcp.EncodeU32(buffer, No);
                ret += Dcp.EncodeU16(buffer, (ushort)DataObjects.Length);
                for (int i = 0; i < DataObjects.Length; i++)
                    ret += DataObjects[i].Serialize(buffer);
                ret += Dcp.EncodeU16(buffer, (ushort)Iocs.Length);
                for (int i = 0; i < Iocs.Length; i++)
                    ret += Iocs[i].Serialize(buffer);
                return ret;
            }
        }

        public class IocrBlockRequest : IProfinetSerialize
        {
            public IocrTypes Type;
            public UInt16 Reference;
            public IocrProperties Properties;
            public UInt16 DataLength;
            public UInt16 FrameId;
            public UInt16 SendClockFactor;
            public UInt16 ReductionRatio;
            public UInt16 Phase;
            public UInt16 Sequence;
            public UInt32 FrameSendOffset;
            public UInt16 DataHoldFactorA;
            public UInt16 DataHoldFactorB;
            public IocrTagHeaders TagHeader;
            public System.Net.NetworkInformation.PhysicalAddress MulticastMac;
            public IocApi[] ApIs;

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                /* BlockHeader, IOCRType, IOCRReference, LT, IOCRProperties, DataLength, FrameID,
                    SendClockFactor, ReductionRatio, Phase, Sequence, FrameSendOffset, DataHoldFactor a,
                    DataHoldFactor, IOCRTagHeader, IOCRMulticastMACAdd, NumberOfAPIs, (API,
                    NumberOfIODataObjects, (SlotNumber, SubslotNumber, IODataObjectFrameOffset)*,
                    NumberOfIOCS, (SlotNumber, SubslotNumber, IOCSFrameOffset)*)* */

                ret += EncodeBlockHeader(buffer, BlockTypes.IocrBlockReq, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, (ushort)Type);
                ret += Dcp.EncodeU16(buffer, Reference);
                ret += Dcp.EncodeU16(buffer, 0x8892);   //LT
                ret += Dcp.EncodeU32(buffer, (uint)Properties);
                ret += Dcp.EncodeU16(buffer, DataLength);
                ret += Dcp.EncodeU16(buffer, FrameId);
                ret += Dcp.EncodeU16(buffer, SendClockFactor);
                ret += Dcp.EncodeU16(buffer, ReductionRatio);
                ret += Dcp.EncodeU16(buffer, Phase);
                ret += Dcp.EncodeU16(buffer, Sequence);
                ret += Dcp.EncodeU32(buffer, FrameSendOffset);
                ret += Dcp.EncodeU16(buffer, DataHoldFactorA);
                ret += Dcp.EncodeU16(buffer, DataHoldFactorB);
                ret += Dcp.EncodeU16(buffer, (ushort)TagHeader);
                ret += Dcp.EncodeOctets(buffer, MulticastMac.GetAddressBytes());
                ret += Dcp.EncodeU16(buffer, (ushort)ApIs.Length);
                for (int i = 0; i < ApIs.Length; i++)
                    ret += ApIs[i].Serialize(buffer);
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }
        }

        public class AlarmCrBlockRequest : IProfinetSerialize
        {
            public AlarmCrProperties Properties;
            public UInt16 RtaTimeoutFactor;
            public UInt16 RtaRetries;
            public UInt16 LocalAlarmReference;
            public UInt16 MaxAlarmDataLength;
            public UInt16 AlarmCrTagHeaderHigh;
            public UInt16 AlarmCrTagHeaderLow;

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                /* BlockHeader, AlarmCRType, LT, AlarmCRProperties, RTATimeoutFactor, RTARetries,
                    LocalAlarmReference, MaxAlarmDataLength, AlarmCRTagHeaderHigh,
                    AlarmCRTagHeaderLow */

                ret += EncodeBlockHeader(buffer, BlockTypes.AlarmCrBlockReq, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, 1);         //AlarmCRType
                ret += Dcp.EncodeU16(buffer, 0x8892);    //LT
                ret += Dcp.EncodeU32(buffer, (UInt32)Properties);
                ret += Dcp.EncodeU16(buffer, RtaTimeoutFactor);
                ret += Dcp.EncodeU16(buffer, RtaRetries);
                ret += Dcp.EncodeU16(buffer, LocalAlarmReference);
                ret += Dcp.EncodeU16(buffer, MaxAlarmDataLength);
                ret += Dcp.EncodeU16(buffer, AlarmCrTagHeaderHigh);
                ret += Dcp.EncodeU16(buffer, AlarmCrTagHeaderLow);
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }
        }

        public class DataDescription : IProfinetSerialize
        {
            public enum Types : ushort
            {
                Input = 1,
                Output = 2,
            }

            public Types Type { get; set; }
            public UInt16 SubmoduleDataLength { get; set; }
            public byte LengthIocs { get; set; }
            public byte LengthIops { get; set; }

            // empty constructor required to build the project
            public DataDescription()
            {
            }

            public DataDescription(Types type, UInt16 submoduleDataLength, byte lengthIocs, byte lengthIops)
            {
                Type = type;
                SubmoduleDataLength = submoduleDataLength;
                LengthIocs = lengthIocs;
                LengthIops = lengthIops;
            }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += Dcp.EncodeU16(buffer, (ushort)Type);
                ret += Dcp.EncodeU16(buffer, SubmoduleDataLength);
                ret += Dcp.EncodeU8(buffer, LengthIocs);
                ret += Dcp.EncodeU8(buffer, LengthIops);
                return ret;
            }
        }

        public class Submodule : IProfinetSerialize
        {
            public UInt16 SubslotNumber { get; set; }
            public UInt32 SubmoduleIdentNumber { get; set; }
            public SubmoduleProperties SubmoduleProperties { get; set; }
            public DataDescription[] DataDescription { get; set; }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += Dcp.EncodeU16(buffer, SubslotNumber);
                ret += Dcp.EncodeU32(buffer, SubmoduleIdentNumber);
                ret += Dcp.EncodeU16(buffer, (ushort)SubmoduleProperties);
                for (int i = 0; i < DataDescription.Length; i++)
                    ret += DataDescription[i].Serialize(buffer);
                return ret;
            }
        }

        public class SubmoduleApi : IProfinetSerialize
        {
            public UInt32 No { get; set; }
            public UInt16 SlotNumber { get; set; }
            public UInt32 ModuleIdentNumber { get; set; }
            public Submodule[] SubModules { get; set; }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += Dcp.EncodeU32(buffer, No);
                ret += Dcp.EncodeU16(buffer, SlotNumber);
                ret += Dcp.EncodeU32(buffer, ModuleIdentNumber);
                ret += Dcp.EncodeU16(buffer, 0);    //ModuleProperties
                ret += Dcp.EncodeU16(buffer, (ushort)SubModules.Length);
                for (int i = 0; i < SubModules.Length; i++)
                    ret += SubModules[i].Serialize(buffer);
                return ret;
            }
        }

        public class ExpectedSubmoduleBlockRequest : IProfinetSerialize
        {
            public SubmoduleApi[] ApIs { get; set; }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                /* BlockHeader, NumberOfAPIs, (API, SlotNumber a, ModuleIdentNumber, ModuleProperties,
                    NumberOfSubmodules, (SubslotNumber, SubmoduleIdentNumber, SubmoduleProperties b,
                    (DataDescription, SubmoduleDataLength, LengthIOPS, LengthIOCS)*)*)* */

                ret += EncodeBlockHeader(buffer, BlockTypes.ExpectedSubmoduleBlockReq, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, (ushort)ApIs.Length);
                for (int i = 0; i < ApIs.Length; i++)
                    ret += ApIs[i].Serialize(buffer);
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }
        }

        public class PrmServerBlock : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class McrBlockRequest : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class ArrpcBlockRequest : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class IrInfoBlock : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class SrInfoBlock : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class ArVendorBlockRequest : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public class ArfsuBlock : IProfinetSerialize
        {
            public int Serialize(System.IO.Stream buffer)
            {
                throw new NotImplementedException();
            }
        }

        public static int EncodeConnectRequest(System.IO.Stream buffer, ArBlockRequest arBlockReq, IocrBlockRequest[] iocrBlockReq, AlarmCrBlockRequest alarmCrBlockReq, ExpectedSubmoduleBlockRequest[] expectedSubmoduleBlockReq, PrmServerBlock prmServerBlock, McrBlockRequest[] mcrBlockReq, ArrpcBlockRequest arrpcBlockReq, IrInfoBlock irInfoBlock, SrInfoBlock srInfoBlock, ArVendorBlockRequest[] arVendorBlockRequest, ArfsuBlock arfsuBlock)
        {
            int ret = 0;

            /* ARBlockReq, {[IOCRBlockReq*], [AlarmCRBlockReq], [ExpectedSubmoduleBlockReq*] b,
                [PrmServerBlock], [MCRBlockReq*] a, [ARRPCBlockReq], [IRInfoBlock], [SRInfoBlock],
                [ARVendorBlockReq*], [ARFSUBlock] } */

            if (arBlockReq == null) throw new ArgumentException("ar_block_req is mandatory");
            ret += arBlockReq.Serialize(buffer);
            if (iocrBlockReq != null)
                foreach (IocrBlockRequest v in iocrBlockReq)
                    ret += v.Serialize(buffer);
            if (alarmCrBlockReq != null)
                ret += alarmCrBlockReq.Serialize(buffer);
            if (expectedSubmoduleBlockReq != null)
                foreach (ExpectedSubmoduleBlockRequest v in expectedSubmoduleBlockReq)
                    ret += v.Serialize(buffer);
            if (prmServerBlock != null)
                ret += prmServerBlock.Serialize(buffer);
            if (mcrBlockReq != null)
                foreach (McrBlockRequest v in mcrBlockReq)
                    ret += v.Serialize(buffer);
            if (arrpcBlockReq != null)
                ret += arrpcBlockReq.Serialize(buffer);
            if (irInfoBlock != null)
                ret += irInfoBlock.Serialize(buffer);
            if (srInfoBlock != null)
                ret += srInfoBlock.Serialize(buffer);
            if (arVendorBlockRequest != null)
                foreach (ArVendorBlockRequest v in arVendorBlockRequest)
                    ret += v.Serialize(buffer);
            if (arfsuBlock != null)
                ret += arfsuBlock.Serialize(buffer);

            return ret;
        }

        public class IodWriteReqHeader : IProfinetSerialize
        {
            public UInt16 SeqNumber;
            public Guid Aruuid = Guid.Empty;
            public UInt32 Api;
            public UInt16 SlotNumber;
            public UInt16 SubslotNumber;
            public Index Index;
            public UInt32 RecordDataLength;

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                ret += EncodeBlockHeader(buffer, BlockTypes.IodWriteReqHeader, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, SeqNumber);
                ret += Rpc.EncodeGuid(buffer, Rpc.Encodings.BigEndian, Aruuid);
                ret += Dcp.EncodeU32(buffer, Api);
                ret += Dcp.EncodeU16(buffer, SlotNumber);
                ret += Dcp.EncodeU16(buffer, SubslotNumber);
                ret += Dcp.EncodeU16(buffer, 0); //padding
                ret += Dcp.EncodeU16(buffer, (ushort)Index);
                ret += Dcp.EncodeU32(buffer, RecordDataLength);
                ret += Dcp.EncodeOctets(buffer, new byte[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0});   //padding
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }

            public override bool Equals(object obj)
            {
                if (!(obj is IodWriteReqHeader)) return false;
                IodWriteReqHeader o = (IodWriteReqHeader)obj;
                return o.Index == Index;
            }

            public override int GetHashCode()
            {
                return Index.GetHashCode();
            }
        }

        public static int EncodeWriteRequest(System.IO.Stream buffer, KeyValuePair<IodWriteReqHeader,byte[]> value)
        {
            int ret = 0;

            ret += value.Key.Serialize(buffer);
            ret += Dcp.EncodeOctets(buffer, value.Value);

            return ret;
        }

        public static int EncodeWriteMultipleRequest(System.IO.Stream buffer, Guid aruuid, IEnumerable<KeyValuePair<IodWriteReqHeader,byte[]>> values)
        {
            int ret = 0;
            ushort no = 1;
            int tmp = 0;

            //encode header
            long headerPosition = buffer.Position;
            IodWriteReqHeader header = new IodWriteReqHeader();
            header.Aruuid = aruuid;
            header.Index = Index.WriteMultiple;
            ret += header.Serialize(buffer);

            foreach (KeyValuePair<IodWriteReqHeader, byte[]> v in values)
            {
                //padding
                while ((tmp % 4) != 0) tmp += Dcp.EncodeU8(buffer, 0);

                if(v.Key.Aruuid == Guid.Empty) v.Key.Aruuid = aruuid;
                v.Key.SeqNumber = no++;
                tmp += v.Key.Serialize(buffer);
                tmp += Dcp.EncodeOctets(buffer, v.Value);
            }

            //re-encode header
            header.RecordDataLength = (uint)tmp;
            long currentPos = buffer.Position;
            buffer.Position = headerPosition;
            header.Serialize(buffer);
            buffer.Position = currentPos;

            return ret + tmp;
        }

        public static int EncodeReadRequest(System.IO.Stream buffer)
        {
            throw new NotImplementedException();
        }

        public class ControlBlockConnect : IProfinetSerialize, IProfinetDeserialize
        {
            public BlockTypes BlockType { get; set; }
            public Guid Aruuid { get; set; }
            public UInt16 SessionKey { get; set; }
            public ControlCommands ControlCommand { get; set; }
            public ControlBlockProperties ControlBlockProperties { get; set; }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                long blockLengthPosition;

                ret += EncodeBlockHeader(buffer, BlockType, 0, out blockLengthPosition);
                ret += Dcp.EncodeU16(buffer, 0);    //padding
                ret += Rpc.EncodeGuid(buffer, Rpc.Encodings.BigEndian, Aruuid);
                ret += Dcp.EncodeU16(buffer, SessionKey);
                ret += Dcp.EncodeU16(buffer, 0);    //padding
                ret += Dcp.EncodeU16(buffer, (ushort)ControlCommand);
                ret += Dcp.EncodeU16(buffer, (ushort)ControlBlockProperties);
                ReEncodeBlockLength(buffer, blockLengthPosition);

                return ret;
            }

            public int Deserialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ushort tmp;
                BlockTypes bt;
                Guid g;

                DecodeBlockHeader(buffer, out bt, out tmp);
                BlockType = bt;
                ret += Dcp.DecodeU16(buffer, out tmp);    //padding
                ret += Rpc.DecodeGuid(buffer, Rpc.Encodings.BigEndian, out g);
                Aruuid = g;
                ret += Dcp.DecodeU16(buffer, out tmp);
                SessionKey = tmp;
                ret += Dcp.DecodeU16(buffer, out tmp);    //padding
                ret += Dcp.DecodeU16(buffer, out tmp);
                ControlCommand = (ControlCommands)tmp;
                ret += Dcp.DecodeU16(buffer, out tmp);
                ControlBlockProperties = (ControlBlockProperties)tmp;

                return ret;
            }
        }

        public static int EncodeControlRequest(System.IO.Stream buffer, ControlBlockConnect controlBlockConnect)
        {
            int ret = 0;

            ret += controlBlockConnect.Serialize(buffer);

            return ret;
        }

        public static int EncodeControlResponse(System.IO.Stream buffer, ControlBlockConnect controlBlockConnect)
        {
            int ret = 0;

            ret += controlBlockConnect.Serialize(buffer);

            return ret;
        }

        public static int EncodeReleaseRequest(System.IO.Stream buffer, Guid aruuid, ushort sessionKey)
        {
            ControlBlockConnect controlBlockConnect = new ControlBlockConnect();
            controlBlockConnect.BlockType = BlockTypes.ReleaseBlockReq;
            controlBlockConnect.ControlCommand = ControlCommands.Release;
            controlBlockConnect.Aruuid = aruuid;
            controlBlockConnect.SessionKey = sessionKey;
                
            int ret = 0;
            ret += controlBlockConnect.Serialize(buffer);
            return ret;
        }

        public class PnioStatus : IProfinetDeserialize
        {
            public ErrorCodes ErrorCode { get; set; }
            public ErrorDecodes ErrorDecode { get; set; }
            public byte ErrorCode1 { get; set; }
            public byte ErrorCode2 { get; set; }

            public enum ErrorCodes : byte
            {
                Ok = 0,
                //0x01 – 0x1F Reserved —
                //0x20 – 0x3F Manufacturer specific Within the LogBookData
                //0x40 – 0x80 Reserved —
                Pnio = 0x81, //Used for all errors not covered elsewhere.
                //0x82 – 0xCE Reserved —
                RtaError = 0xCF, //Within the ERR-RTA-PDU and UDP-RTA-PDU
                //0xD0 – 0xD9 Reserved —
                AlarmAck = 0xDA, //Within the DATA-RTA-PDU and UDP-RTA-PDU
                IodConnectRes = 0xDB, //Within the CL-RPC-PDU
                IodReleaseRes = 0xDC, //Within the CL-RPC-PDU
                OxControlRes = 0xDD, //Within the CL-RPC-PDU
                IodReadRes = 0xDE, //Within the CL-RPC-PDU only used with ErrorDecode=PNIORW
                IodWriteRes = 0xDF //Within the CL-RPC-PDU only used with ErrorDecode=PNIORW
                //0xE0 – 0xEF Reserved —
                //0xF0 – 0xFF Reserved
            }

            public enum ErrorDecodes : byte
            {
                Ok = 0,
                //0x01 – 0x7F Reserved —
                Pniorw = 0x80, //Used in context with user error codes of the services Read and Write
                Pnio = 0x81, //Used in context with other services or internal e.g. RPC errors
                ManufacturerSpecific = 0x82, //Used only in context of LogBookData
                //0x83 – 0xFF Reserved
            }

            public enum ErrorCode1Pniorw : byte
            {
                Ok = 0,

                ApplicationReadError = 0xA0,
                ApplicationWriteError = 0xA1,
                ApplicationModuleFailure = 0xA2,
                ApplicationBusy = 0xA7,
                ApplicationVersionConflict = 0xA8,
                ApplicationFeatureNotSupported = 0xA9,
                ApplicationUserSpecific1 = 0xAA,
                ApplicationUserSpecific2 = 0xAB,
                ApplicationUserSpecific3 = 0xAC,
                ApplicationUserSpecific4 = 0xAD,
                ApplicationUserSpecific5 = 0xAE,
                ApplicationUserSpecific6 = 0xAF,

                AccessInvalidIndex = 0xB0,
                AccessWriteLengthError= 0xB1,
                AccessInvalidSlot= 0xB2,
                AccessTypeConflict= 0xB3,
                AccessInvalidArea= 0xB4,
                AccessStateConflict= 0xB5,
                AccessAccessDenied= 0xB6,
                AccessInvalidRange= 0xB7,
                AccessInvalidParameter= 0xB8,
                AccessInvalidType= 0xB9,
                AccessBackup= 0xBA,
                AccessUserSpecific7= 0xBB,
                AccessUserSpecific8= 0xBC,
                AccessUserSpecific9= 0xBD,
                AccessUserSpecific10= 0xBE,
                AccessUserSpecific11= 0xBF,

                ResourceReadConstrainConflict = 0xC0,
                ResourceWriteConstrainConflict = 0xC1,
                ResourceBusy = 0xC2,
                ResourceUnavailable = 0xC3,
                ResourceUserSpecific12 = 0xC8,
                ResourceUserSpecific13 = 0xC9,
                ResourceUserSpecific14 = 0xCA,
                ResourceUserSpecific15 = 0xCB,
                ResourceUserSpecific16 = 0xCC,
                ResourceUserSpecific17 = 0xCD,
                ResourceUserSpecific18 = 0xCE,
                ResourceUserSpecific19 = 0xCF,
            }

            public enum ErrorCode1Pnio
            {
                Ok = 0,

                FaultyArBlockReq = 1,
                FaultyIocrBlockReq = 2,
                FaultyExpectedSubmoduleBlockReq = 3,
                FaultyAlarmCrBlockReq = 4,
                FaultyPrmServerBlockReq = 5,
                FaultyMcrBlockReq = 6,
                FaultyArrpcBlockReq = 7,
                FaultyRecord = 8,
                FaultyIrInfoBlock = 9,
                FaultySrInfoBlock = 10,
                FaultyArfsuBlock = 11,
                FaultyArVendorBlockReq = 12,
                FaultyControlBlockConnect = 20,
                FaultyControlBlockPlug = 21,
                FaultyControlBlockConnectAfterConnectionEstablishment = 22,
                FaultyControlBlockPlugAfterPlugAlarm = 23,
                FaultyControlBlockPrmBegin = 24,
                FaultySubmoduleListBlock = 25,
                FaultyReleaseBlock = 40,
                FaultyArBlockRes = 50,
                FaultyIocrBlockRes = 51,
                FaultyAlarmCrBlockRes = 52,
                FaultyModuleDiffBlock = 53,
                FaultyArrpcBlockRes = 54,
                FaultyArServerBlockRes = 55,
                FaultyArVendorBlockRes = 56,
                AlarmAckError = 60,
                Cmdev = 61,
                Cmctl = 62,
                Ctldina = 63,
                Cmrpc = 64,
                Alpmi = 65,
                Alpmr = 66,
                Lmpm = 67,
                Mac = 68,
                Rpc = 69,
                Apmr = 70,
                Apms = 71,
                Cpm = 72,
                Ppm = 73,
                Dcpucs = 74,
                Dcpucr = 75,
                Dcpmcs = 76,
                Dcpmcr = 77,
                Fspm = 78,
                Ctlsm = 100,
                Ctlrdi = 101,
                Ctlrdr = 102,
                Ctlwri = 103,
                Ctlwrr = 104,
                Ctlio = 105,
                Ctlsu = 106,
                Ctlrpc = 107,
                Ctlpbe = 108,
                Ctlsrl = 109,
                Cmsm = 200,
                Cmrdr = 202,
                Cmwrr = 204,
                Cmio = 205,
                Cmsu = 206,
                Cmina = 208,
                Cmpbe = 209,
                Cmdmc = 210,
                Cmsrl = 211,
                RtaErrClsProtocol = 253,
            }

            public int Deserialize(System.IO.Stream buffer)
            {
                int ret = 0;
                byte tmp;

                ret += Dcp.DecodeU8(buffer, out tmp);
                ErrorCode = (ErrorCodes)tmp;
                ret += Dcp.DecodeU8(buffer, out tmp);
                ErrorDecode = (ErrorDecodes)tmp;
                ret += Dcp.DecodeU8(buffer, out tmp);
                ErrorCode1 = tmp;
                ret += Dcp.DecodeU8(buffer, out tmp);
                ErrorCode2 = tmp;

                return ret;
            }

            public override string ToString()
            {
                string str = ErrorCode.ToString() + " - " + ErrorDecode.ToString() + " - ";

                if (ErrorDecode == ErrorDecodes.Pniorw)
                {
                    ErrorCode1Pniorw tmp = (ErrorCode1Pniorw)ErrorCode1;
                    str += tmp.ToString() + " - " + ErrorCode2.ToString();
                }
                else if (ErrorDecode == ErrorDecodes.Pnio)
                {
                    ErrorCode1Pnio tmp = (ErrorCode1Pnio)ErrorCode1;
                    str += tmp.ToString() + " - " + ErrorCode2.ToString();
                }
                else
                {
                    str += ErrorCode1.ToString() + " - " + ErrorCode2.ToString();
                }
                return str;
            }
        }
    }
}
