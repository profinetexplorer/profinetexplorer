﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Profinet
{
    public class Rpc
    {
        public static Guid UuidIoObjectInstanceXyz = Guid.Parse("DEA00000-6C97-11D1-8271-000000000000");
        public static Guid UuidIoDeviceInterface = Guid.Parse("DEA00001-6C97-11D1-8271-00A02442DF7D");
        public static Guid UuidIoControllerInterface = Guid.Parse("DEA00002-6C97-11D1-8271-00A02442DF7D");
        public static Guid UuidIoSupervisorInterface = Guid.Parse("DEA00003-6C97-11D1-8271-00A02442DF7D");
        public static Guid UuidIoParameterServerInterface = Guid.Parse("DEA00004-6C97-11D1-8271-00A02442DF7D");
        public static Guid UuidEpMapInterface = Guid.Parse("E1AF8308-5D1F-11C9-91A4-08002B14A0FA");
        public static Guid UuidEpMapObject = Guid.Parse("00000000-0000-0000-0000-000000000000");

        public enum PacketTypes : byte
        {
            Request = 0,
            Ping = 1,
            Response = 2,
            Fault = 3,
            Working = 4,
            NoCall = 5,
            Reject = 6,
            Acknowledge = 7,
            ConnectionlessCancel = 8,
            FragmentAcknowledge = 9,
            CancelAcknowledge = 10,
        }

        [Flags]
        public enum Flags1 : byte
        {
            LastFragment = 2,
            Fragment = 4,
            NoFragmentAckRequested = 8,
            Maybe = 16,
            Idempotent = 32,
            Broadcast = 64,
        }

        [Flags]
        public enum Flags2 : byte
        {
            CancelPendingAtCallEnd = 2,
        }

        [Flags]
        public enum Encodings : ushort
        {
            Ascii = 0x000,
            Ebcdic = 0x100,
            BigEndian = 0x000,
            LittleEndian = 0x1000,
            IeeeFloat = 0,
            VaxFloat = 1,
            CrayFloat = 2,
            IbmFloat = 3,
        }

        public enum Operations : ushort
        {
            //IO device
            Connect = 0,
            Release = 1,
            Read = 2,
            Write = 3,
            Control = 4,
            ReadImplicit = 5,

            //Endpoint mapper
            Insert = 0,
            Delete = 1,
            Lookup = 2,
            Map = 3,
            LookupHandleFree = 4,
            InqObject = 5,
            MgmtDelete = 6,
        }

        public static Guid GenerateObjectInstanceUuid(UInt16 instanceNo, byte interfaceNo, UInt16 deviceId, UInt16 vendorId)
        {
            byte[] bytes = UuidIoObjectInstanceXyz.ToByteArray();
            UInt32 data1 = BitConverter.ToUInt32(bytes, 0);
            UInt16 data2 = BitConverter.ToUInt16(bytes, 4);
            UInt16 data3 = BitConverter.ToUInt16(bytes, 6);
            byte data4 = bytes[8];
            byte data5 = bytes[9];
            instanceNo &= 0xFFF;
            instanceNo |= (UInt16)(interfaceNo << 12);
            Guid ret = new Guid(data1, data2, data3, data4, data5, (byte)(instanceNo >> 8), (byte)(instanceNo & 0xFF), (byte)(deviceId >> 8), (byte)(deviceId & 0xFF), (byte)(vendorId >> 8), (byte)(vendorId & 0xFF));
            return ret;
        }

        public static int EncodeU32(System.IO.Stream buffer, Encodings encoding, UInt32 value)
        {
            if ((encoding & Encodings.LittleEndian) == Encodings.BigEndian)
            {
                buffer.WriteByte((byte)((value & 0xFF000000) >> 24));
                buffer.WriteByte((byte)((value & 0x00FF0000) >> 16));
                buffer.WriteByte((byte)((value & 0x0000FF00) >> 08));
                buffer.WriteByte((byte)((value & 0x000000FF) >> 00));
            }
            else
            {
                buffer.WriteByte((byte)((value & 0x000000FF) >> 00));
                buffer.WriteByte((byte)((value & 0x0000FF00) >> 08));
                buffer.WriteByte((byte)((value & 0x00FF0000) >> 16));
                buffer.WriteByte((byte)((value & 0xFF000000) >> 24));
            }
            return 4;
        }

        public static int DecodeU32(System.IO.Stream buffer, Encodings encoding, out UInt32 value)
        {
            if ((encoding & Encodings.LittleEndian) == Encodings.BigEndian)
            {
                value = (UInt32)((buffer.ReadByte() << 24) | (buffer.ReadByte() << 16) | (buffer.ReadByte() << 8) | (buffer.ReadByte() << 0));
            }
            else
            {
                value = (UInt32)((buffer.ReadByte() << 0) | (buffer.ReadByte() << 8) | (buffer.ReadByte() << 16) | (buffer.ReadByte() << 24));
            }
            return 4;
        }

        public static int EncodeU16(System.IO.Stream buffer, Encodings encoding, UInt16 value)
        {
            if ((encoding & Encodings.LittleEndian) == Encodings.BigEndian)
            {
                buffer.WriteByte((byte)((value & 0x0000FF00) >> 08));
                buffer.WriteByte((byte)((value & 0x000000FF) >> 00));
            }
            else
            {
                buffer.WriteByte((byte)((value & 0x000000FF) >> 00));
                buffer.WriteByte((byte)((value & 0x0000FF00) >> 08));
            }
            return 2;
        }

        public static int DecodeU16(System.IO.Stream buffer, Encodings encoding, out UInt16 value)
        {
            if ((encoding & Encodings.LittleEndian) == Encodings.BigEndian)
            {
                value = (UInt16)((buffer.ReadByte() << 8) | (buffer.ReadByte() << 0));
            }
            else
            {
                value = (UInt16)((buffer.ReadByte() << 0) | (buffer.ReadByte() << 8));
            }
            return 2;
        }

        public static int EncodeGuid(System.IO.Stream buffer, Encodings encoding, Guid value)
        {
            int ret = 0;
            byte[] bytes = value.ToByteArray();
            UInt32 data1 = BitConverter.ToUInt32(bytes, 0);
            UInt16 data2 = BitConverter.ToUInt16(bytes, 4);
            UInt16 data3 = BitConverter.ToUInt16(bytes, 6);
            byte[] data4 = new byte[8];
            Array.Copy(bytes, 8, data4, 0, 8);
            ret += EncodeU32(buffer, encoding, data1);
            ret += EncodeU16(buffer, encoding, data2);
            ret += EncodeU16(buffer, encoding, data3);
            ret += Dcp.EncodeOctets(buffer, data4);
            return ret;
        }

        public static int DecodeGuid(System.IO.Stream buffer, Encodings encoding, out Guid value)
        {
            int ret = 0;
            UInt32 data1;
            UInt16 data2;
            UInt16 data3;
            byte[] data4 = new byte[8];

            ret += DecodeU32(buffer, encoding, out data1);
            ret += DecodeU16(buffer, encoding, out data2);
            ret += DecodeU16(buffer, encoding, out data3);
            buffer.Read(data4, 0, data4.Length);
            ret += data4.Length;

            value = new Guid((int)data1, (short)data2, (short)data3, data4);

            return ret;
        }

        public static int EncodeHeader(System.IO.Stream buffer, PacketTypes type, Flags1 flags1, Flags2 flags2, Encodings encoding, UInt16 serialHighLow, Guid objectId, Guid interfaceId, Guid activityId, UInt32 serverBootTime, UInt32 sequenceNo, Operations op, UInt16 bodyLength, UInt16 fragmentNo, out long bodyLengthPosition)
        {
            int ret = 0;

            ret += Dcp.EncodeU8(buffer, 4); //RPCVersion
            ret += Dcp.EncodeU8(buffer, (byte)type);
            ret += Dcp.EncodeU8(buffer, (byte)flags1);
            ret += Dcp.EncodeU8(buffer, (byte)flags2);
            ret += Dcp.EncodeU16(buffer, (ushort)encoding);
            ret += Dcp.EncodeU8(buffer, 0); //pad
            ret += Dcp.EncodeU8(buffer, (byte)(serialHighLow >> 8));
            ret += EncodeGuid(buffer, encoding, objectId);
            ret += EncodeGuid(buffer, encoding, interfaceId);
            ret += EncodeGuid(buffer, encoding, activityId);
            ret += EncodeU32(buffer, encoding, serverBootTime);
            ret += EncodeU32(buffer, encoding, 1);   //interface version
            ret += EncodeU32(buffer, encoding, sequenceNo);
            ret += EncodeU16(buffer, encoding, (ushort)op);
            ret += EncodeU16(buffer, encoding, 0xFFFF);     //interface hint
            ret += EncodeU16(buffer, encoding, 0xFFFF);     //activity hint
            bodyLengthPosition = buffer.Position;
            ret += EncodeU16(buffer, encoding, bodyLength);
            ret += EncodeU16(buffer, encoding, fragmentNo);
            ret += Dcp.EncodeU8(buffer, 0); //authentication protocol
            ret += Dcp.EncodeU8(buffer, (byte)(serialHighLow & 0xFF));

            return ret;
        }

        public static int DecodeHeader(System.IO.Stream buffer, out PacketTypes type, out Flags1 flags1, out Flags2 flags2, out Encodings encoding, out UInt16 serialHighLow, out Guid objectId, out Guid interfaceId, out Guid activityId, out UInt32 serverBootTime, out UInt32 sequenceNo, out Operations op, out UInt16 bodyLength, out UInt16 fragmentNo)
        {
            int ret = 0;
            byte tmp1;
            UInt16 tmp2;
            UInt32 tmp3;

            serialHighLow = 0;

            ret += Dcp.DecodeU8(buffer, out tmp1); //RPCVersion
            if (tmp1 != 4) throw new Exception("Wrong protocol");
            ret += Dcp.DecodeU8(buffer, out tmp1);
            type = (PacketTypes)tmp1;
            ret += Dcp.DecodeU8(buffer, out tmp1);
            flags1 = (Flags1)tmp1;
            ret += Dcp.DecodeU8(buffer, out tmp1);
            flags2 = (Flags2)tmp1;
            ret += Dcp.DecodeU16(buffer, out tmp2);
            encoding = (Encodings)tmp2;
            ret += Dcp.DecodeU8(buffer, out tmp1); //pad
            ret += Dcp.DecodeU8(buffer, out tmp1);
            serialHighLow |= (UInt16)(tmp1 << 8);
            ret += DecodeGuid(buffer, encoding, out objectId);
            ret += DecodeGuid(buffer, encoding, out interfaceId);
            ret += DecodeGuid(buffer, encoding, out activityId);
            ret += DecodeU32(buffer, encoding, out serverBootTime);
            ret += DecodeU32(buffer, encoding, out tmp3);   //interface version
            if ((tmp3 & 0xFFFF) != 1) throw new Exception("Wrong protocol version");
            ret += DecodeU32(buffer, encoding, out sequenceNo);
            ret += DecodeU16(buffer, encoding, out tmp2);
            op = (Operations)tmp2;
            ret += DecodeU16(buffer, encoding, out tmp2);     //interface hint
            ret += DecodeU16(buffer, encoding, out tmp2);     //activity hint
            ret += DecodeU16(buffer, encoding, out bodyLength);
            ret += DecodeU16(buffer, encoding, out fragmentNo);
            ret += Dcp.DecodeU8(buffer, out tmp1); //authentication protocol
            ret += Dcp.DecodeU8(buffer, out tmp1);
            serialHighLow |= tmp1;

            return ret;
        }

        public static void ReEncodeHeaderLength(System.IO.Stream buffer, Encodings encoding, long bodyLengthPosition)
        {
            long currentPos = buffer.Position;
            UInt16 actualLength = (UInt16)(buffer.Position - bodyLengthPosition - 6);

            buffer.Position = bodyLengthPosition;
            EncodeU16(buffer, encoding, actualLength);

            buffer.Position = currentPos;
        }

        public static int EncodeNdrDataHeader(System.IO.Stream buffer, Encodings encoding, UInt32 argsMaximumOrPnioStatus, UInt32 argsLength, UInt32 maximumCount, UInt32 offset, UInt32 actualCount, out long ndrDataHeaderPosition)
        {
            int ret = 0;

            ndrDataHeaderPosition = buffer.Position;
            ret += EncodeU32(buffer, encoding, argsMaximumOrPnioStatus);
            ret += EncodeU32(buffer, encoding, argsLength);
            ret += EncodeU32(buffer, encoding, maximumCount);
            ret += EncodeU32(buffer, encoding, offset);
            ret += EncodeU32(buffer, encoding, actualCount);

            return ret;
        }

        public static int DecodeNdrDataHeader(System.IO.Stream buffer, Encodings encoding, out UInt32 argsMaximumOrPnioStatus, out UInt32 argsLength, out UInt32 maximumCount, out UInt32 offset, out UInt32 actualCount)
        {
            int ret = 0;

            ret += DecodeU32(buffer, encoding, out argsMaximumOrPnioStatus);
            ret += DecodeU32(buffer, encoding, out argsLength);
            ret += DecodeU32(buffer, encoding, out maximumCount);
            ret += DecodeU32(buffer, encoding, out offset);
            ret += DecodeU32(buffer, encoding, out actualCount);

            return ret;
        }

        public static void ReEncodeNdrDataHeaderLength(System.IO.Stream buffer, Encodings encoding, long ndrDataHeaderPosition, bool reEncodePniostatus)
        {
            long currentPos = buffer.Position;
            UInt32 actualLength = (UInt32)(buffer.Position - ndrDataHeaderPosition - 20);

            buffer.Position = ndrDataHeaderPosition;
            if (reEncodePniostatus) EncodeU32(buffer, encoding, actualLength);     //ArgsMaximum/PNIOStatus
            else buffer.Position += 4;
            EncodeU32(buffer, encoding, actualLength);     //ArgsLength
            EncodeU32(buffer, encoding, actualLength);     //MaximumCount
            buffer.Position += 4;
            EncodeU32(buffer, encoding, actualLength);     //ActualCount

            buffer.Position = currentPos;
        }
    }
}
