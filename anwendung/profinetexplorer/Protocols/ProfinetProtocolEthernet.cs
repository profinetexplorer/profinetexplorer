﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Profinet
{
    public class Ethernet
    {
        public enum Type : ushort
        {
            None = 0,
            Loop = 96,
            Echo = 512,
            IpV4 = 2048,
            Arp = 2054,
            WakeOnLan = 2114,
            ReverseArp = 32821,
            AppleTalk = 32923,
            AppleTalkArp = 33011,
            VLanTaggedFrame = 33024,
            NovellInternetworkPacketExchange = 33079,
            Novell = 33080,
            IpV6 = 34525,
            MacControl = 34824,
            CobraNet = 34841,
            MultiprotocolLabelSwitchingUnicast = 34887,
            MultiprotocolLabelSwitchingMulticast = 34888,
            PointToPointProtocolOverEthernetDiscoveryStage = 34915,
            PointToPointProtocolOverEthernetSessionStage = 34916,
            ExtensibleAuthenticationProtocolOverLan = 34958,
            HyperScsi = 34970,
            AtaOverEthernet = 34978,
            EtherCatProtocol = 34980,
            ProviderBridging = 34984,
            AvbTransportProtocol = 34997,
            Lldp = 35020,
            SerialRealTimeCommunicationSystemIii = 35021,
            CircuitEmulationServicesOverEthernet = 35032,
            HomePlug = 35041,
            MacSecurity = 35045,
            PrecisionTimeProtocol = 35063,
            ConnectivityFaultManagementOrOperationsAdministrationManagement = 35074,
            FibreChannelOverEthernet = 35078,
            FibreChannelOverEthernetInitializationProtocol = 35092,
            QinQ = 37120,
            VeritasLowLatencyTransport = 51966,
        }

        public static int Encode(System.IO.Stream buffer, System.Net.NetworkInformation.PhysicalAddress destination, System.Net.NetworkInformation.PhysicalAddress source, Type type)
        {
            //destination
            Dcp.EncodeOctets(buffer, destination.GetAddressBytes());

            //source
            Dcp.EncodeOctets(buffer, source.GetAddressBytes());

            //type
            Dcp.EncodeU16(buffer, (ushort)type);

            return 14;
        }
    }
}
