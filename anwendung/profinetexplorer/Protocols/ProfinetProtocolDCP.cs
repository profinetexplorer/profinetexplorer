﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;

namespace Profinet
{
    public class Dcp
    {
        [Flags]
        public enum ServiceIds : ushort
        {
            GetRequest = 0x0300,
            GetResponse = 0x0301,
            SetRequest = 0x0400,
            SetResponse = 0x0401,
            IdentifyRequest = 0x0500,
            IdentifyResponse = 0x0501,
            HelloRequest = 0x0600,
            ServiceIdNotSupported = 0x0004,
        }

        public enum BlockOptions : ushort
        {
            //IP
            IpMacAddress = 0x0101,
            IpIpParameter = 0x0102,
            IpFullIpSuite = 0x0103,

            //DeviceProperties
            DevicePropertiesDeviceVendor = 0x0201,
            DevicePropertiesNameOfStation = 0x0202,
            DevicePropertiesDeviceId = 0x0203,
            DevicePropertiesDeviceRole = 0x0204,
            DevicePropertiesDeviceOptions = 0x0205,
            DevicePropertiesAliasName = 0x0206,
            DevicePropertiesDeviceInstance = 0x0207,
            DevicePropertiesOemDeviceId = 0x0208,

            //DHCP
            DhcpHostName = 0x030C,
            DhcpVendorSpecificInformation = 0x032B,
            DhcpServerIdentifier = 0x0336,
            DhcpParameterRequestList = 0x0337,
            DhcpClassIdentifier = 0x033C,
            DhcpDhcpClientIdentifier = 0x033D,
            DhcpFullyQualifiedDomainName = 0x0351,
            DhcpUuidClientIdentifier = 0x0361,
            DhcpDhcp = 0x03FF,

            //Control
            ControlStart = 0x0501,
            ControlStop = 0x0502,
            ControlSignal = 0x0503,
            ControlResponse = 0x0504,
            ControlFactoryReset = 0x0505,
            ControlResetToFactory = 0x0506,

            //DeviceInitiative
            DeviceInitiativeDeviceInitiative = 0x0601,

            //AllSelector
            AllSelectorAllSelector = 0xFFFF,
        }

        public enum BlockQualifiers : ushort
        {
            Temporary = 0,
            Permanent = 1,

            ResetApplicationData = 2,
            ResetCommunicationParameter = 4,
            ResetEngineeringParameter = 6,
            ResetAllStoredData = 8,
            ResetDevice = 16,
            ResetAndRestoreData = 18,
        }

        [Flags]
        public enum BlockInfo : ushort
        {
            IpSet = 1,
            IpSetViaDhcp = 2,
            IpConflict = 0x80,
        }

        [Flags]
        public enum DeviceRoles : byte
        {
            Device = 1,
            Controller = 2,
            Multidevice = 4,
            Supervisor = 8,
        }

        public enum BlockErrors : byte
        {
            NoError = 0,
            OptionNotSupported = 1,
            SuboptionNotSupported = 2,
            SuboptionNotSet = 3,
            ResourceError = 4,
            SetNotPossible = 5,
            Busy = 6,
        }

        public static int EncodeU32(System.IO.Stream buffer, UInt32 value)
        {
            buffer.WriteByte((byte)((value & 0xFF000000) >> 24));
            buffer.WriteByte((byte)((value & 0x00FF0000) >> 16));
            buffer.WriteByte((byte)((value & 0x0000FF00) >> 08));
            buffer.WriteByte((byte)((value & 0x000000FF) >> 00));
            return 4;
        }

        public static int EncodeU16(System.IO.Stream buffer, UInt16 value)
        {
            buffer.WriteByte((byte)((value & 0xFF00) >> 08));
            buffer.WriteByte((byte)((value & 0x00FF) >> 00));
            return 2;
        }

        public static int EncodeU8(System.IO.Stream buffer, byte value)
        {
            buffer.WriteByte((byte)value);
            return 1;
        }

        public static int DecodeU16(System.IO.Stream buffer, out UInt16 value)
        {
            if (buffer.Position >= buffer.Length)
            {
                value = 0;
                return 0;
            }
            value = (UInt16)((buffer.ReadByte() << 8) | buffer.ReadByte());
            return 2;
        }

        public static int DecodeU8(System.IO.Stream buffer, out byte value)
        {
            if (buffer.Position >= buffer.Length)
            {
                value = 0;
                return 0;
            }
            value = (byte)buffer.ReadByte();
            return 1;
        }

        public static int DecodeU32(System.IO.Stream buffer, out UInt32 value)
        {
            if (buffer.Position >= buffer.Length)
            {
                value = 0;
                return 0;
            }
            value = (UInt32)((buffer.ReadByte() << 24) | (buffer.ReadByte() << 16) | (buffer.ReadByte() << 8) | buffer.ReadByte());
            return 4;
        }

        public static int EncodeString(System.IO.Stream buffer, string value)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(value);
            buffer.Write(bytes, 0, bytes.Length);
            return bytes.Length;
        }

        public static int DecodeString(System.IO.Stream buffer, int length, out string value)
        {
            byte[] tmp = new byte[length];
            buffer.Read(tmp, 0, length);
            value = Encoding.ASCII.GetString(tmp);
            return tmp.Length;
        }

        public static int EncodeOctets(System.IO.Stream buffer, byte[] value)
        {
            if (value == null || value.Length == 0) return 0;
            buffer.Write(value, 0, value.Length);
            return value.Length;
        }

        public static int DecodeOctets(System.IO.Stream buffer, int length, out byte[] value)
        {
            if (length <= 0)
            {
                value = null;
                return 0;
            }
            value = new byte[length];
            buffer.Read(value, 0, length);
            return value.Length;
        }

        public static int EncodeHeader(System.IO.Stream buffer, ServiceIds serviceId, UInt32 xid, UInt16 responseDelayFactor, UInt16 dcpDataLength)
        {
            long dummy;
            return EncodeHeader(buffer, serviceId, xid, responseDelayFactor, dcpDataLength, out dummy);
        }

        public static int EncodeHeader(System.IO.Stream buffer, ServiceIds serviceId, UInt32 xid, UInt16 responseDelayFactor, UInt16 dcpDataLength, out long dcpDataLengthPos)
        {
            EncodeU16(buffer, (ushort)serviceId);

            //big endian uint32
            EncodeU32(buffer, xid);

            //ResponseDelayFactor, 1 = Allowed value without spread, 2 – 0x1900 = Allowed value with spread
            EncodeU16(buffer, responseDelayFactor);

            dcpDataLengthPos = buffer.Position;
            EncodeU16(buffer, dcpDataLength);

            return 10;
        }

        public static void ReEncodeDcpDataLength(System.IO.Stream buffer, long dcpDataLengthPos)
        {
            long currentPos = buffer.Position;
            buffer.Position = dcpDataLengthPos;
            EncodeU16(buffer, (ushort)(currentPos - buffer.Position - 2));
            buffer.Position = currentPos;
        }

        public static int DecodeHeader(System.IO.Stream buffer, out ServiceIds serviceId, out UInt32 xid, out UInt16 responseDelayFactor, out UInt16 dcpDataLength)
        {
            ushort val;
            DecodeU16(buffer, out val);
            serviceId = (ServiceIds)val;
            //big endian uint32
            DecodeU32(buffer, out xid);
            //ResponseDelayFactor, 1 = Allowed value without spread, 2 – 0x1900 = Allowed value with spread
            DecodeU16(buffer, out responseDelayFactor);
            DecodeU16(buffer, out dcpDataLength);
            return 10;
        }

        public static int EncodeBlock(System.IO.Stream buffer, BlockOptions options, UInt16 dcpBlockLength)
        {
            long dummy;
            return EncodeBlock(buffer, options, dcpBlockLength, out dummy);
        }

        public static int EncodeBlock(System.IO.Stream buffer, BlockOptions options, UInt16 dcpBlockLength, out long dcpBlockLengthPos)
        {
            EncodeU16(buffer, (ushort)options);
            dcpBlockLengthPos = buffer.Position;
            EncodeU16(buffer, dcpBlockLength);
            return 4;
        }
  
        public static int DecodeBlock(System.IO.Stream buffer, out BlockOptions options, out UInt16 dcpBlockLength)
        {
            ushort opt;
            DecodeU16(buffer, out opt);
            options = (BlockOptions)opt;
            DecodeU16(buffer, out dcpBlockLength);
            return 4;
        }

        public static int EncodeIdentifyResponse(System.IO.Stream buffer, UInt32 xid, Dictionary<Dcp.BlockOptions, object> blocks)
        {
            int ret = 0;
            long dcpDataLengthPos;

            //Header
            ret += EncodeHeader(buffer, ServiceIds.IdentifyResponse, xid, 0, 0, out dcpDataLengthPos);

            //{ IdentifyResBlock, NameOfStationBlockRes,IPParameterBlockRes, DeviceIDBlockRes, DeviceVendorBlockRes,DeviceOptionsBlockRes, DeviceRoleBlockRes, [DeviceInitiativeBlockRes],[DeviceInstanceBlockRes], [OEMDeviceIDBlockRes] }

            foreach (KeyValuePair<Dcp.BlockOptions, object> entry in blocks)
            {
                ret += EncodeNextBlock(buffer, entry);
            }

            //adjust dcp_length
            ReEncodeDcpDataLength(buffer, dcpDataLengthPos);

            return ret;
        }

        /// <summary>
        /// This is a helper class for the block options
        /// </summary>
        [TypeConverter(typeof(ExpandableObjectConverter)), Serializable]
        public class BlockOptionMeta
        {
            public string Name { get; set; }
            public bool IsReadable { get; set; }
            public bool IsWriteable { get; set; }

            [Browsable(false), System.Xml.Serialization.XmlIgnore]
            public BlockOptions BlockOption { get; set; }

            public byte Option
            {
                get
                {
                    return (byte)((((UInt16)BlockOption) & 0xFF00) >> 8);
                }
                set
                {
                    BlockOption = (BlockOptions)((((UInt16)BlockOption) & 0x00FF) | ((UInt16)value << 8));
                }
            }

            public byte SubOption
            {
                get
                {
                    return (byte)((((UInt16)BlockOption) & 0x00FF) >> 0);
                }
                set
                {
                    BlockOption = (BlockOptions)((((UInt16)BlockOption) & 0xFF00) | ((UInt16)value << 0));
                }
            }

            // empty constructor required to build the project
            private BlockOptionMeta()   //For XmlSerializer
            {
            }

            public BlockOptionMeta(string name, byte option, byte subOption, bool isReadable, bool isWriteable)
            {
                this.BlockOption = 0;
                this.Name = name;
                this.Option = option;
                this.SubOption = subOption;
                this.IsReadable = isReadable;
                this.IsWriteable = isWriteable;
            }

            public BlockOptionMeta(BlockOptions opt)
            {
                BlockOption = opt;
                Name = opt.ToString();
                if (opt == BlockOptions.IpMacAddress ||
                    opt == BlockOptions.DevicePropertiesDeviceId ||
                    opt == BlockOptions.DevicePropertiesDeviceVendor ||
                    opt == BlockOptions.DevicePropertiesDeviceRole ||
                    opt == BlockOptions.DevicePropertiesDeviceOptions ||
                    opt == BlockOptions.DevicePropertiesDeviceInstance ||
                    opt == BlockOptions.DevicePropertiesOemDeviceId ||
                    opt == BlockOptions.DeviceInitiativeDeviceInitiative)
                {
                    IsReadable = true;
                }
                else if (opt == BlockOptions.DevicePropertiesAliasName ||
                         opt == BlockOptions.ControlResponse ||
                         opt == BlockOptions.AllSelectorAllSelector)
                {
                    //none
                }
                else if (opt == BlockOptions.ControlStart ||
                         opt == BlockOptions.ControlStop ||
                         opt == BlockOptions.ControlSignal ||
                         opt == BlockOptions.ControlFactoryReset ||
                         opt == BlockOptions.ControlResetToFactory)
                {
                    IsWriteable = true;
                }
                else
                {
                    //default
                    IsReadable = true;
                    IsWriteable = true;
                }
            }
        }

        public class IpAddressConverter : TypeConverter
        {
            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                if (sourceType == typeof(string)) return true;
                return base.CanConvertFrom(context, sourceType);
            }
            public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
            {
                if (value is string)
                    return System.Net.IPAddress.Parse((string)value);
                return base.ConvertFrom(context, culture, value);
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class IpInfo : IProfinetSerialize
        {
            public BlockInfo Info { get; set; }
            [TypeConverter(typeof(IpAddressConverter))]
            public System.Net.IPAddress Ip { get; set; }
            [TypeConverter(typeof(IpAddressConverter))]
            public System.Net.IPAddress SubnetMask { get; set; }
            [TypeConverter(typeof(IpAddressConverter))]
            public System.Net.IPAddress Gateway { get; set; }
            public IpInfo(BlockInfo info, byte[] ip, byte[] subnet, byte[] gateway)
            {
                Info = info;
                Ip = new System.Net.IPAddress(ip);
                SubnetMask = new System.Net.IPAddress(subnet);
                Gateway = new System.Net.IPAddress(gateway);
            }
            public override string ToString()
            {
                return "{" + Ip.ToString() + " - " + SubnetMask.ToString() + " - " + Gateway.ToString() + "}";
            }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                byte[] tmp;
                tmp = Ip.GetAddressBytes();
                buffer.Write(tmp, 0, tmp.Length);
                ret += tmp.Length;
                tmp = SubnetMask.GetAddressBytes();
                buffer.Write(tmp, 0, tmp.Length);
                ret += tmp.Length;
                tmp = Gateway.GetAddressBytes();
                buffer.Write(tmp, 0, tmp.Length);
                ret += tmp.Length;
                return ret;
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class DeviceIdInfo : IProfinetSerialize
        {
            public UInt16 VendorId { get; set; }
            public UInt16 DeviceId { get; set; }
            public DeviceIdInfo(UInt16 vendorId, UInt16 deviceId)
            {
                VendorId = vendorId;
                DeviceId = deviceId;
            }
            public override string ToString()
            {
                return "Vendor 0x" + VendorId.ToString("X") + " - Device 0x" + DeviceId.ToString("X");
            }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += EncodeU16(buffer, VendorId);
                ret += EncodeU16(buffer, DeviceId);
                return ret;
            }
            public override bool Equals(object obj)
            {
                if (!(obj is DeviceIdInfo)) return false;
                DeviceIdInfo o = (DeviceIdInfo)obj;
                return o.DeviceId == DeviceId && o.VendorId == VendorId;
            }
            public override int GetHashCode()
            {
                UInt32 tmp = (uint)(VendorId << 16);
                tmp |= DeviceId;
                return tmp.GetHashCode();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class DeviceRoleInfo : IProfinetSerialize
        {
            public DeviceRoles DeviceRole { get; set; }

            public DeviceRoleInfo(DeviceRoles deviceRole)
            {
                DeviceRole = deviceRole;
            }

            public int Serialize(System.IO.Stream buffer)
            {
                int ret = 0;
                ret += EncodeU8(buffer, (byte)DeviceRole);
                buffer.WriteByte(0);    //padding
                ret++;
                return ret;
            }

            public override string ToString()
            {
                return DeviceRole.ToString();
            }
        }

        [TypeConverter(typeof(ExpandableObjectConverter))]
        public class ResponseStatus
        {
            public BlockOptions Option { get; set; }
            public BlockErrors Error { get; set; }
            // empty constructor required to build the project
            public ResponseStatus()
            {
            }
            public ResponseStatus(BlockOptions option, BlockErrors error)
            {
                this.Option = option;
                this.Error = error;
            }
            public override string ToString()
            {
                return Error.ToString();
            }
        }

        private static int EncodeNextBlock(System.IO.Stream buffer, KeyValuePair<BlockOptions, object> block)
        {
            int ret = 0;
            int tmp = 0;
            long dcpBlockLengthPos;

            ret += EncodeBlock(buffer, block.Key, 0, out dcpBlockLengthPos);

            //block info
            if (block.Value != null) ret += EncodeU16(buffer, 0);
            else return ret;

            if (block.Value is ResponseStatus)
            {
                buffer.Position -= 2;
                EncodeU16(buffer, (ushort)((ResponseStatus)block.Value).Option);
                tmp += EncodeU8(buffer, (byte)((ResponseStatus)block.Value).Error);
            }
            else if (block.Value is IProfinetSerialize)
            {
                tmp += ((IProfinetSerialize)block.Value).Serialize(buffer);
            }
            else if (block.Value is string)
            {
                tmp += EncodeString(buffer, (string)block.Value);
            }
            else if (block.Value is byte[])
            {
                tmp += EncodeOctets(buffer, (byte[])block.Value);
            }
            else if (block.Value is BlockOptions[])
            {
                foreach (BlockOptions b in (BlockOptions[])block.Value)
                {
                    tmp += EncodeU16(buffer, (ushort)b);
                }
            }
            else
            {
                throw new NotImplementedException();
            }

            //adjust length
            ReEncodeDcpDataLength(buffer, dcpBlockLengthPos);

            //padding
            ret += tmp;
            if ((tmp % 2) != 0)
            {
                buffer.WriteByte(0);
                ret++;
            }

            return ret;
        }

        private static int DecodeNextBlock(System.IO.Stream buffer, ushort dcpLength, out KeyValuePair<BlockOptions, object> block)
        {
            int ret = 0;
            BlockOptions options;
            UInt16 dcpBlockLength;
            UInt16 blockInfo = 0;
            UInt16 tmp, tmp2;
            string str;
            object content;
            ResponseStatus setResponse;

            if (buffer.Position >= buffer.Length || dcpLength <= 0)
            {
                block = new KeyValuePair<BlockOptions, object>(0, null);
                return ret;
            }

            ret += DecodeBlock(buffer, out options, out dcpBlockLength);
            if (dcpBlockLength >= 2) ret += DecodeU16(buffer, out blockInfo);
            dcpBlockLength -= 2;

            switch (options)
            {
                case BlockOptions.DevicePropertiesNameOfStation:
                    ret += DecodeString(buffer, dcpBlockLength, out str);
                    content = str;
                    break;
                case BlockOptions.IpIpParameter:
                    byte[] ip, subnet, gateway;
                    ret += DecodeOctets(buffer, 4, out ip);
                    ret += DecodeOctets(buffer, 4, out subnet);
                    ret += DecodeOctets(buffer, 4, out gateway);
                    content = new IpInfo((BlockInfo)blockInfo, ip, subnet, gateway); ;
                    break;
                case BlockOptions.DevicePropertiesDeviceId:
                    ret += DecodeU16(buffer, out tmp);
                    ret += DecodeU16(buffer, out tmp2);
                    content = new DeviceIdInfo(tmp, tmp2);
                    break;
                case BlockOptions.DevicePropertiesDeviceOptions:
                    BlockOptions[] optionList = new BlockOptions[dcpBlockLength / 2];
                    for (int i = 0; i < optionList.Length; i++)
                    {
                        ret += DecodeU16(buffer, out tmp);
                        optionList[i] = (BlockOptions)tmp;
                    }
                    content = optionList;
                    break;
                case BlockOptions.DevicePropertiesDeviceRole:
                    DeviceRoles roles = (DeviceRoles)buffer.ReadByte();
                    buffer.ReadByte(); //padding
                    ret += 2;
                    content = new DeviceRoleInfo(roles);
                    break;
                case BlockOptions.DevicePropertiesDeviceVendor:
                    ret += DecodeString(buffer, dcpBlockLength, out str);
                    content = str;
                    break;
                case BlockOptions.ControlResponse:
                    setResponse = new ResponseStatus();
                    setResponse.Option = (BlockOptions)blockInfo;
                    setResponse.Error = (BlockErrors)buffer.ReadByte();
                    ret++;
                    content = setResponse;
                    break;
                default:
                    byte[] arr;
                    ret += DecodeOctets(buffer, dcpBlockLength, out arr);
                    content = arr;
                    break;
            }
            block = new KeyValuePair<BlockOptions, object>(options, content);

            //padding
            if ((dcpBlockLength % 2) != 0)
            {
                buffer.ReadByte();
                ret++;
            }

            return ret;
        }

        public static int DecodeAllBlocks(System.IO.Stream buffer, ushort dcpLength, out Dictionary<BlockOptions, object> blocks)
        {
            int ret = 0, r;
            KeyValuePair<Profinet.Dcp.BlockOptions, object> value;
            blocks = new Dictionary<BlockOptions, object>();
            while ((r = Profinet.Dcp.DecodeNextBlock(buffer, (ushort)(dcpLength - ret), out value)) > 0)
            {
                ret += r;
                if (!blocks.ContainsKey(value.Key)) blocks.Add(value.Key, value.Value);
                else Utilities.EventLogging.logEvent(null, "Multiple blocks in reply: " + value.Key, false, "error");
            }
            if (r < 0) return r;    //error
            else return ret;
        }

        public static int EncodeIdentifyRequest(System.IO.Stream buffer, UInt32 xid)
        {
            int ret = 0;

            //Header
            ret += EncodeHeader(buffer, ServiceIds.IdentifyRequest, xid, 1, 4);

            //optional filter (instead of the ALL block)
            //[NameOfStationBlock] ^ [AliasNameBlock], IdentifyReqBlock

            //IdentifyReqBlock
            /*  DeviceRoleBlock ^ DeviceVendorBlock ^ DeviceIDBlock ^
                DeviceOptionsBlock ^ OEMDeviceIDBlock ^ MACAddressBlock ^
                IPParameterBlock ^ DHCPParameterBlock ^
                ManufacturerSpecificParameterBlock */

            //AllSelectorType
            ret += EncodeBlock(buffer, BlockOptions.AllSelectorAllSelector, 0);

            return ret;
        }

        public static int EncodeSetRequest(System.IO.Stream buffer, UInt32 xid, BlockOptions options, BlockQualifiers qualifiers, byte[] data)
        {
            int ret = 0;
            int dataLength = 0;
            bool doPad = false;

            if (data != null) dataLength = data.Length;
            if ((dataLength % 2) != 0) doPad = true;

            //The following is modified by F.Chaxel. 
            //TODO: Test that decode still work

            ret += EncodeHeader(buffer, ServiceIds.SetRequest, xid, 0, (ushort)(12 + dataLength + (doPad ? 1 : 0)));
            ret += EncodeBlock(buffer, options, (ushort)(2 + dataLength));

            ret += EncodeU16(buffer, (ushort)0); // Don't care

            //data
            EncodeOctets(buffer, data);

            //pad (re-ordered by f.chaxel)
            if (doPad) ret += EncodeU8(buffer, 0);

            //BlockQualifier
            ret += EncodeBlock(buffer, BlockOptions.ControlStop, (ushort)(2 + dataLength));
            ret += EncodeU16(buffer, (ushort)qualifiers);

            return ret;
        }

        public static int EncodeGetRequest(System.IO.Stream buffer, UInt32 xid, BlockOptions options)
        {
            int ret = 0;

            ret += EncodeHeader(buffer, ServiceIds.GetRequest, xid, 0, 2);
            ret += EncodeU16(buffer, (ushort)options);

            return ret;
        }

        public static int EncodeHelloRequest(System.IO.Stream buffer, UInt32 xid)
        {
            throw new NotImplementedException();
        }

        public static int EncodeGetResponse(System.IO.Stream buffer, UInt32 xid, BlockOptions options, object data)
        {
            int ret = 0;
            long dcpLength;

            ret += EncodeHeader(buffer, ServiceIds.GetResponse, xid, 0, 0, out dcpLength);
            ret += EncodeNextBlock(buffer, new KeyValuePair<BlockOptions, object>(options, data));
            ReEncodeDcpDataLength(buffer, dcpLength);

            return ret;
        }

        public static int EncodeSetResponse(System.IO.Stream buffer, UInt32 xid, BlockOptions options, BlockErrors status)
        {
            int ret = 0;
            long dcpLength;

            ret += EncodeHeader(buffer, ServiceIds.SetResponse, xid, 0, 0, out dcpLength);
            ret += EncodeNextBlock(buffer, new KeyValuePair<BlockOptions, object>(BlockOptions.ControlResponse, new ResponseStatus(options, status)));
            ReEncodeDcpDataLength(buffer, dcpLength);

            return ret;
        }
    }
}