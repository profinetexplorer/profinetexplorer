﻿/**************************************************************************
*                           MIT License
* 
* Copyright (C) 2014 Morten Kvistgaard <mk@pch-engineering.dk>
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be included
* in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*
*********************************************************************/

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Diagnostics;


namespace Profinet
{
    public class Rt
    {
        public enum FrameIds : ushort
        {
            PtcpRtSyncPduWithFollowUp = 0x20,
            PtcpRtSyncPdu = 0x80,
            AlarmHigh = 0xFC01,
            AlarmLow = 0xFE01,
            DcpHelloReqPdu = 0xFEFC,
            DcpGetSetPdu = 0xFEFD,
           DcpIdentifyReqPdu = 0xFEFE,
            DcpIdentifyResPdu = 0xFEFF,
            PtcpAnnouncePdu = 0xFF00,
            PtcpFollowUpPdu = 0xFF20,
            PtcpDelayReqPdu = 0xFF40,
            PtcpDelayResPduWithFollowUp = 0xFF41,
            PtcpDelayFuResPduWithFollowUp = 0xFF42,
            PtcpDelayResPdu = 0xFF43,
            RtcStart = 0xC000,
            RtcEnd = 0xF7FF,
        }

        public const string MulticastMacAddIdentifyAddress = "01-0E-CF-00-00-00";
        public const string MulticastMacAddHelloAddress = "01-0E-CF-00-00-01";
        public const string MulticastMacAddRange1DestinationAddress = "01-0E-CF-00-01-01";
        public const string MulticastMacAddRange1InvalidAddress = "01-0E-CF-00-01-02";
        public const string PtcpMulticastMacAddRange2ClockSynchronizationAddress = "01-0E-CF-00-04-00";
        public const string PtcpMulticastMacAddRange3ClockSynchronizationAddress = "01-0E-CF-00-04-20";
        public const string PtcpMulticastMacAddRange4ClockSynchronizationAddress = "01-0E-CF-00-04-40";
        public const string PtcpMulticastMacAddRange6ClockSynchronizationAddress = "01-0E-CF-00-04-80";
        public const string PtcpMulticastMacAddRange8Address = "01-80-C2-00-00-0E";
        public const string RtcPduRtClass3DestinationAddress = "01-0E-CF-00-01-01";
        public const string RtcPduRtClass3InvalidAddress = "01-0E-CF-00-01-02";

        public static int EncodeFrameId(System.IO.Stream buffer, FrameIds value)
        {
            return Dcp.EncodeU16(buffer, (ushort)value);
        }

        public static int DecodeFrameId(System.IO.Stream buffer, out FrameIds value)
        {
            ushort val;
            Dcp.DecodeU16(buffer, out val);
            value = (FrameIds)val;
            return 2;
        }

        [Flags]
        public enum DataStatus : byte
        {
            StatePrimary = 1,  /* 0 is Backup */
            RedundancyBackup = 2,  /* 0 is Primary */
            DataItemValid = 4,  /* 0 is invalid */
            ProviderStateRun = 1 << 4, /* 0 is stop */
            StationProblemIndicatorNormal = 1 << 5,    /* 0 is Detected */
            Ignore = 1 << 7,    /* 0 is Evaluate */
        }

        [Flags]
        public enum TransferStatus : byte
        {
            Ok = 0,
            AlignmentOrFrameChecksumError = 1,
            WrongLengthError = 2,
            MacReceiveBufferOverflow = 4,
            RtClass3Error = 8,
        }

        [Flags]
        public enum IoxS : byte
        {
            ExtensionMoreIoxSOctetFollows = 1,
            InstanceDetectedBySubslot = 0 << 5,
            InstanceDetectedBySlot = 1 << 5,
            InstanceDetectedByIoDevice = 2 << 5,
            InstanceDetectedByIoController = 3 << 5,
            DataStateGood = 1 << 7,
        }

        [Flags]
        public enum PduTypes : byte
        {
            //0x00 Reserved —
            Data = 1, //Shall only be used to encode the DATA-RTA-PDU
            Nack = 2, //Shall only be used to encode the NACK-RTA-PDU
            Ack = 3, //Shall only be used to encode the ACK-RTA-PDU
            Err = 4, //Shall only be used to encode the ERR-RTA-PDU
            //0x05 – 0x0F Reserved —
            Version1 = 1 << 4,
        }

        [Flags]
        public enum AddFlags : byte
        {
            WindowSizeOne = 1,
            TackImmediateAcknowledge = 1 << 4,
        }

        public static int DecodeRtcStatus(System.IO.Stream buffer, out UInt16 cycleCounter, out DataStatus dataStatus, out TransferStatus transferStatus)
        {
            int ret = 0;
            byte tmp;

            ret += Dcp.DecodeU16(buffer, out cycleCounter);
            ret += Dcp.DecodeU8(buffer, out tmp);
            dataStatus = (DataStatus)tmp;
            ret += Dcp.DecodeU8(buffer, out tmp);
            transferStatus = (TransferStatus)tmp;

            return ret;
        }

        public static int EncodeRtcStatus(System.IO.Stream buffer, UInt16 cycleCounter, DataStatus dataStatus, TransferStatus transferStatus)
        {
            int ret = 0;

            ret += Dcp.EncodeU16(buffer, cycleCounter);
            ret += Dcp.EncodeU8(buffer, (byte)dataStatus);
            ret += Dcp.EncodeU8(buffer, (byte)transferStatus);

            return ret;
        }

        public static int DecodeRtaHeader(System.IO.Stream buffer, out UInt16 alarmDestinationEndpoint, out UInt16 alarmSourceEndpoint, out PduTypes pduType, out AddFlags addFlags, out UInt16 sendSeqNum, out UInt16 ackSeqNum, out UInt16 varPartLen)
        {
            int ret = 0;
            byte tmp;

            ret += Dcp.DecodeU16(buffer, out alarmDestinationEndpoint);
            ret += Dcp.DecodeU16(buffer, out alarmSourceEndpoint);
            ret += Dcp.DecodeU8(buffer, out tmp);
            pduType = (PduTypes)tmp;
            ret += Dcp.DecodeU8(buffer, out tmp);
            addFlags = (AddFlags)tmp;
            ret += Dcp.DecodeU16(buffer, out sendSeqNum);
            ret += Dcp.DecodeU16(buffer, out ackSeqNum);
            ret += Dcp.DecodeU16(buffer, out varPartLen);

            return ret;
        }
    }
}
