using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Utilities
{
    /// <summary>
	/// Dynamic Property GridContainer
	/// </summary>
	class DynamicPropertyGridContainer : CollectionBase,ICustomTypeDescriptor
	{
		/// <summary>
		/// Add CustomProperty to Collectionbase List
		/// </summary>
		/// <param name="value"></param>
		public void Add(CustomProperty value)
		{
			base.List.Add(value);
		}

		/// <summary>
		/// Remove item from List
		/// </summary>
		/// <param name="name"></param>
		public void Remove(string name)
		{
			foreach(CustomProperty prop in base.List)
			{
				if(prop.Name == name)
				{
					base.List.Remove(prop);
					return;
				}
			}
		}

		/// <summary>
		/// Indexer
		/// </summary>
		public CustomProperty this[int index] 
		{
			get 
			{
				return (CustomProperty)base.List[index];
			}
			set
			{
				base.List[index] = (CustomProperty)value;
			}
		}

        public CustomProperty this[string name]
        {
            get
            {
                foreach (CustomProperty p in this)
                {
                    if (p.Name == name) return p;
                }
                return null;
            }
        }

		#region "TypeDescriptor Implementation"
		/// <summary>
		/// Get Class Name
		/// </summary>
		/// <returns>String</returns>
		public String GetClassName()
		{
			return TypeDescriptor.GetClassName(this,true);
		}

		/// <summary>
		/// GetAttributes
		/// </summary>
		/// <returns>AttributeCollection</returns>
		public AttributeCollection GetAttributes()
		{
			return TypeDescriptor.GetAttributes(this,true);
		}

		/// <summary>
		/// GetComponentName
		/// </summary>
		/// <returns>String</returns>
		public String GetComponentName()
		{
			return TypeDescriptor.GetComponentName(this, true);
		}

		/// <summary>
		/// GetConverter
		/// </summary>
		/// <returns>TypeConverter</returns>
		public TypeConverter GetConverter()
		{
			return TypeDescriptor.GetConverter(this, true);
		}

		/// <summary>
		/// GetDefaultEvent
		/// </summary>
		/// <returns>EventDescriptor</returns>
		public EventDescriptor GetDefaultEvent() 
		{
			return TypeDescriptor.GetDefaultEvent(this, true);
		}

		/// <summary>
		/// GetDefaultProperty
		/// </summary>
		/// <returns>PropertyDescriptor</returns>
		public PropertyDescriptor GetDefaultProperty() 
		{
			return TypeDescriptor.GetDefaultProperty(this, true);
		}

		/// <summary>
		/// GetEditor
		/// </summary>
		/// <param name="editorBaseType">editorBaseType</param>
		/// <returns>object</returns>
		public object GetEditor(Type editorBaseType) 
		{
			return TypeDescriptor.GetEditor(this, editorBaseType, true);
		}

		public EventDescriptorCollection GetEvents(Attribute[] attributes) 
		{
			return TypeDescriptor.GetEvents(this, attributes, true);
		}

		public EventDescriptorCollection GetEvents()
		{
			return TypeDescriptor.GetEvents(this, true);
		}

		public PropertyDescriptorCollection GetProperties(Attribute[] attributes)
		{
			PropertyDescriptor[] newProps = new PropertyDescriptor[this.Count];
			for (int i = 0; i < this.Count; i++)
			{
				CustomProperty  prop = (CustomProperty) this[i];
				newProps[i] = new CustomPropertyDescriptor(ref prop, attributes);
			}

			return new PropertyDescriptorCollection(newProps);
		}

		public PropertyDescriptorCollection GetProperties()
		{
			
			return TypeDescriptor.GetProperties(this, true);
			
		}

		public object GetPropertyOwner(PropertyDescriptor pd) 
		{
			return this;
		}
		#endregion

        public override string ToString()
        {
            return "Custom type";
        }
	}

	/// <summary>
	/// Custom property class 
	/// </summary>
	class CustomProperty
	{
		private string _mName = string.Empty;
		private bool _mReadonly = false;
        private object _mOldValue = null;
		private object _mValue = null;
        private Type _mType;
        private object _mTag;
        private DynamicEnum _mOptions;
        private string _mCategory;
        private string _mDescription;

        public CustomProperty(string name, object value, Type type, bool readOnly, string category = "", string description = "", DynamicEnum options = null, object tag = null)
		{
			this._mName = name;
            this._mOldValue = value;
			this._mValue = value;
            this._mType = type;
			this._mReadonly = readOnly;
            this._mTag = tag;
            this._mOptions = options;
            this._mCategory = category;
            this._mDescription = description;
		}

        public DynamicEnum Options
        {
            get { return _mOptions; }
        }

        public Type Type
        {
            get { return _mType; }
        }

        public string Category
        {
            get { return _mCategory; }
        }

        public string Description
        {
            get { return _mDescription; }
        }

		public bool ReadOnly
		{
			get
			{
				return _mReadonly;
			}
		}

		public string Name
		{
			get
			{
				return _mName;
			}
		}

		public bool Visible
		{
			get
			{
				return true;
			}
		}

		public object Value
		{
			get
			{
				return _mValue;
			}
			set
			{
				_mValue = value;
			}
		}

        public object Tag
        {
            get { return _mTag; }
        }

        public void Reset()
        {
            _mValue = _mOldValue;
        }
	}
}
