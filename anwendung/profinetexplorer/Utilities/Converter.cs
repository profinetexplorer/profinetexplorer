﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace Utilities
{
    /// <summary>
	/// IntToHex TypeConverter
	/// </summary>
    public class IntToHexTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            else
            {
                return base.CanConvertFrom(context, sourceType);
            }
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
            {
                return true;
            }
            else
            {
                return base.CanConvertTo(context, destinationType);
            }
        }

        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) && (value.GetType() == typeof(byte) || value.GetType() == typeof(sbyte)))
            {
                return string.Format("0x{0:X2}", value);
            }
            else if (destinationType == typeof(string) && (value.GetType() == typeof(short) || value.GetType() == typeof(ushort)))
            {
                return string.Format("0x{0:X4}", value);
            }
            else if (destinationType == typeof(string) && (value.GetType() == typeof(int) || value.GetType() == typeof(uint)))
            {
                return string.Format("0x{0:X8}", value);
            }
            else if (destinationType == typeof(string) && (value.GetType() == typeof(Int64) || value.GetType() == typeof(UInt64)))
            {
                return string.Format("0x{0:X16}", value);
            }
            else
            {
                return base.ConvertTo(context, culture, value, destinationType);
            }
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value.GetType() == typeof(string))
            {
                string input = (string)value;

                if (input.StartsWith("0x", StringComparison.OrdinalIgnoreCase))
                {
                    input = input.Substring(2);
                    return Convert.ChangeType(int.Parse(input, System.Globalization.NumberStyles.HexNumber, culture), context.PropertyDescriptor.PropertyType);
                }
                else
                    return base.ConvertFrom(context, culture, int.Parse(input, System.Globalization.NumberStyles.HexNumber, culture));
            }
            else
            {
                return base.ConvertFrom(context, culture, value);
            }
        }
    }

    /// <summary>
	/// Dynamic EnumConverter
	/// </summary>
    public class DynamicEnumConverter : TypeConverter
    {
        // Fields
        private DynamicEnum _mE;

        public DynamicEnumConverter(DynamicEnum e)
        {
            _mE = e;
        }

        private static bool is_number(string str)
        {
            if (string.IsNullOrWhiteSpace(str) || str.Length == 0) return false;
            for (int i = 0; i < str.Length; i++)
                if (!char.IsNumber(str, i)) return false;
            return true;
        }

        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string && value != null)
            {
                string str = (string)value;
                str = str.Trim();

                if (_mE.Contains(str)) return _mE[str];
                else if (is_number(str))
                {
                    int intVal;
                    if (str.StartsWith("0x", StringComparison.InvariantCultureIgnoreCase))
                        intVal = int.Parse(str.Substring(2), System.Globalization.NumberStyles.HexNumber);
                    else
                        intVal = int.Parse(str);
                    return intVal;
                }
                else
                {
                    return _mE[str];
                }
            }
            return base.ConvertFrom(context, culture, value);
        }
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return true;
        }
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return true;
        }
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == null)
                throw new ArgumentNullException("destinationType");

            if ((destinationType == typeof(string)) && (value != null))
            {
                if (value is string)
                {
                    return value;
                }
                else if (value is KeyValuePair<string, int>)
                    return ((KeyValuePair<string, int>)value).Key;

                int val = (int)Convert.ChangeType(value, typeof(int));
                return _mE[val];
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            return new TypeConverter.StandardValuesCollection(_mE);
        }

        public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
        {
            return !_mE.IsFlag;
        }

        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override bool IsValid(ITypeDescriptorContext context, object value)
        {
            if (value is string) return _mE.Contains((string)value);

            int val = (int)Convert.ChangeType(value, typeof(int));
            return _mE.Contains(val);
        }
    }

    /// <summary>
	/// Custom SingleConverter
	/// </summary>
    public class CustomSingleConverter : SingleConverter
    {
        public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
        {
            if ((destinationType == typeof(string)) && (value.GetType() == typeof(float)))
            {
                return DoubleConverter.ToExactString((double)(float)value);
            }

            return base.ConvertTo(context, culture, value, destinationType);
        }

    }

    /// <summary>
    /// A class to allow the conversion of doubles to string representations of
    /// their exact decimal values. The implementation aims for readability over
    /// efficiency.
    /// </summary>
    public class DoubleConverter
    {
        /// <summary>
        /// Converts the given double to a string representation of its
        /// exact decimal value.
        /// </summary>
        /// <param name="d">The double to convert.</param>
        /// <returns>A string representation of the double's exact decimal value.</return>
        public static string ToExactString(double d)
        {
            if (double.IsPositiveInfinity(d))
                return System.Globalization.NumberFormatInfo.CurrentInfo.PositiveInfinitySymbol;
            if (double.IsNegativeInfinity(d))
                return System.Globalization.NumberFormatInfo.CurrentInfo.NegativeInfinitySymbol;
            if (double.IsNaN(d))
                return System.Globalization.NumberFormatInfo.CurrentInfo.NaNSymbol;

            // Translate the double into sign, exponent and mantissa.
            long bits = BitConverter.DoubleToInt64Bits(d);
            // Note that the shift is sign-extended, hence the test against -1 not 1
            bool negative = (bits < 0);
            int exponent = (int)((bits >> 52) & 0x7ffL);
            long mantissa = bits & 0xfffffffffffffL;

            // Subnormal numbers; exponent is effectively one higher,
            // but there's no extra normalisation bit in the mantissa
            if (exponent == 0)
            {
                exponent++;
            }
            // Normal numbers; leave exponent as it is but add extra
            // bit to the front of the mantissa
            else
            {
                mantissa = mantissa | (1L << 52);
            }

            // Bias the exponent. It's actually biased by 1023, but we're
            // treating the mantissa as m.0 rather than 0.m, so we need
            // to subtract another 52 from it.
            exponent -= 1075;

            if (mantissa == 0)
            {
                return "0";
            }

            /* Normalize */
            while ((mantissa & 1) == 0)
            {    /*  i.e., Mantissa is even */
                mantissa >>= 1;
                exponent++;
            }

            /// Construct a new decimal expansion with the mantissa
            ArbitraryDecimal ad = new ArbitraryDecimal(mantissa);

            // If the exponent is less than 0, we need to repeatedly
            // divide by 2 - which is the equivalent of multiplying
            // by 5 and dividing by 10.
            if (exponent < 0)
            {
                for (int i = 0; i < -exponent; i++)
                    ad.MultiplyBy(5);
                ad.Shift(-exponent);
            }
            // Otherwise, we need to repeatedly multiply by 2
            else
            {
                for (int i = 0; i < exponent; i++)
                    ad.MultiplyBy(2);
            }

            // Finally, return the string with an appropriate sign
            if (negative)
                return "-" + ad.ToString();
            else
                return ad.ToString();
        }

        /// <summary>Private class used for manipulating
        class ArbitraryDecimal
        {
            /// <summary>Digits in the decimal expansion, one byte per digit
            byte[] _digits;
            /// <summary> 
            /// How many digits are *after* the decimal point
            /// </summary>
            int _decimalPoint = 0;

            /// <summary> 
            /// Constructs an arbitrary decimal expansion from the given long.
            /// The long must not be negative.
            /// </summary>
            internal ArbitraryDecimal(long x)
            {
                string tmp = x.ToString(System.Globalization.CultureInfo.InvariantCulture);
                _digits = new byte[tmp.Length];
                for (int i = 0; i < tmp.Length; i++)
                    _digits[i] = (byte)(tmp[i] - '0');
                Normalize();
            }

            /// <summary>
            /// Multiplies the current expansion by the given amount, which should
            /// only be 2 or 5.
            /// </summary>
            internal void MultiplyBy(int amount)
            {
                byte[] result = new byte[_digits.Length + 1];
                for (int i = _digits.Length - 1; i >= 0; i--)
                {
                    int resultDigit = _digits[i] * amount + result[i + 1];
                    result[i] = (byte)(resultDigit / 10);
                    result[i + 1] = (byte)(resultDigit % 10);
                }
                if (result[0] != 0)
                {
                    _digits = result;
                }
                else
                {
                    Array.Copy(result, 1, _digits, 0, _digits.Length);
                }
                Normalize();
            }

            /// <summary>
            /// Shifts the decimal point; a negative value makes
            /// the decimal expansion bigger (as fewer digits come after the
            /// decimal place) and a positive value makes the decimal
            /// expansion smaller.
            /// </summary>
            internal void Shift(int amount)
            {
                _decimalPoint += amount;
            }

            /// <summary>
            /// Removes leading/trailing zeroes from the expansion.
            /// </summary>
            internal void Normalize()
            {
                int first;
                for (first = 0; first < _digits.Length; first++)
                    if (_digits[first] != 0)
                        break;
                int last;
                for (last = _digits.Length - 1; last >= 0; last--)
                    if (_digits[last] != 0)
                        break;

                if (first == 0 && last == _digits.Length - 1)
                    return;

                byte[] tmp = new byte[last - first + 1];
                for (int i = 0; i < tmp.Length; i++)
                    tmp[i] = _digits[i + first];

                _decimalPoint -= _digits.Length - (last + 1);
                _digits = tmp;
            }

            /// <summary>
            /// Converts the value to a proper decimal string representation.
            /// </summary>
            public override String ToString()
            {
                char[] digitString = new char[_digits.Length];
                for (int i = 0; i < _digits.Length; i++)
                    digitString[i] = (char)(_digits[i] + '0');

                // Simplest case - nothing after the decimal point,
                // and last real digit is non-zero, eg value=35
                if (_decimalPoint == 0)
                {
                    return new string(digitString);
                }

                // Fairly simple case - nothing after the decimal
                // point, but some 0s to add, eg value=350
                if (_decimalPoint < 0)
                {
                    return new string(digitString) +
                           new string('0', -_decimalPoint);
                }

                // Nothing before the decimal point, eg 0.035
                if (_decimalPoint >= digitString.Length)
                {
                    return "0" + System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator +
                        new string('0', (_decimalPoint - digitString.Length)) +
                        new string(digitString);
                }

                // Most complicated case - part of the string comes
                // before the decimal point, part comes after it,
                // eg 3.5
                return new string(digitString, 0,
                                   digitString.Length - _decimalPoint) +
                    System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator +
                    new string(digitString,
                                digitString.Length - _decimalPoint,
                                _decimalPoint);
            }
        }
    }


    public class DynamicEnum : ICollection
    {
        private Dictionary<string, int> _mStringIndex = new Dictionary<string, int>();
        private Dictionary<int, string> _mIntIndex = new Dictionary<int, string>();
        public bool IsFlag { get; set; }

        public int this[string name]
        {
            get
            {
                int value = 0;

                if (name.IndexOf(',') != -1)
                {
                    int num = 0;
                    foreach (string str2 in name.Split(new char[] { ',' }))
                    {
                        _mStringIndex.TryGetValue(str2.Trim(), out value);
                        num |= value;
                    }
                    return num;
                }

                _mStringIndex.TryGetValue(name, out value);
                return value;
            }
        }
        public string this[int value]
        {
            get
            {
                if (IsFlag)
                {
                    string str = "";
                    foreach (KeyValuePair<string, int> entry in _mStringIndex)
                    {
                        if ((value & entry.Value) > 0 || (entry.Value == 0 && value == 0)) str += ", " + entry.Key;
                    }
                    if (str != "") str = str.Substring(2);
                    return str;
                }
                else
                {
                    string name;
                    _mIntIndex.TryGetValue(value, out name);
                    return name;
                }
            }
        }
        public void Add(string name, int value)
        {
            _mStringIndex.Add(name, value);
            _mIntIndex.Add(value, name);
        }
        public bool Contains(string name)
        {
            return _mStringIndex.ContainsKey(name);
        }
        public bool Contains(int value)
        {
            return _mIntIndex.ContainsKey(value);
        }

        public IEnumerator GetEnumerator()
        {
            return _mStringIndex.GetEnumerator();
        }

        public int Count
        {
            get { return _mStringIndex.Count; }
        }

        public void CopyTo(Array array, int index)
        {
            int i = 0;
            foreach (KeyValuePair<string, int> entry in this)
                array.SetValue(entry, i++ + index);
        }

        public bool IsSynchronized
        {
            get { throw new NotImplementedException(); }
        }

        public object SyncRoot
        {
            get { throw new NotImplementedException(); }
        }
    }
}