﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace ProfinetExplorer
{
    public partial class ComboDialog : Form
    {
        public ComboDialog()
        {
            InitializeComponent();
        }

        public string LabelText { get { return label1.Text; } set { label1.Text = value; } }
    }
}
