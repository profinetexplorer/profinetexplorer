﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Profinet;

namespace ProfinetExplorer.MainComponent
{
    class Network
    {
        public ProfinetEthernetTransport EthernetTransport;
        public ProfinetUdpTransport UdpTransport;
        public Dictionary<string, Dictionary<Dcp.BlockOptions, object>> Devices = new Dictionary<string, Dictionary<Dcp.BlockOptions, object>>();
        public Network(ProfinetEthernetTransport ethTransport, ProfinetUdpTransport udpTransport)
        {
            EthernetTransport = ethTransport;
            UdpTransport = udpTransport;
        }
    }

    class Networks
    {
        private Dictionary<string, Network> _networksList = new Dictionary<string, Network>();
        private TreeView _treeView;

        public Dictionary<string, Network>.ValueCollection Values => _networksList.Values;

        public Networks(TreeView treeView)
        {
            _treeView = treeView;
        }

        #region Add new Network

        public void AddNetwork(SharpPcap.ICaptureDevice pcap, bool addToSettings)
        {
            AddNetworkToList(pcap);
            AddNetworkToTree(pcap);

            if (addToSettings)
            {
                if (Properties.Settings.Default.Networks == null)
                {
                    Properties.Settings.Default.Networks = new System.Collections.Specialized.StringCollection();
                }

                Properties.Settings.Default.Networks.Add(pcap.Name);
            }

            Utilities.EventLogging.logEvent(null, "Added " + pcap.Description, false, "information");
        }

        private void AddNetworkToList(SharpPcap.ICaptureDevice pcap)
        {
            if (_networksList.ContainsKey(pcap.Name))
            {
                _networksList.Remove(pcap.Name);
            }

            _networksList.Add(pcap.Name, new Network(new ProfinetEthernetTransport(pcap), null));
        }

        private void AddNetworkToTree(SharpPcap.ICaptureDevice pcap)
        {
            if (_networksList.ContainsKey(pcap.Name))
            {
                _treeView.Nodes.RemoveByKey(pcap.Name);
            }

            var desc = pcap.Description;
            if (desc.StartsWith("Network adapter "))
            {
                desc = desc.Remove(0, 16);
            }

            TreeNode node = _treeView.Nodes[0].Nodes.Add(pcap.Name, desc, 3);

            node.SelectedImageIndex = node.ImageIndex;
            
            _treeView.ExpandAll();
            _treeView.SelectedNode = node;
        }

        #endregion

        #region Remove Network

        public void RemoveNetwork(string key)
        {
            RemoveNetworkFromList(key);
            RemoveNetworkFromTree(key);

            if (Properties.Settings.Default.Networks != null)
            {
                Properties.Settings.Default.Networks.Remove(key);
            }
            Utilities.EventLogging.logEvent(null, "Network removed", false, "information");
        }

        private void RemoveNetworkFromList(string key)
        {
            if (_networksList.ContainsKey(key))
            {
                _networksList.Remove(key);
            }
        }

        private void RemoveNetworkFromTree(string key)
        {
            _treeView.Nodes[0].Nodes.RemoveByKey(key);
        }

        #endregion

        public Network GetNetwork(string key)
        {
            return _networksList[key];
        }

        public bool ContainsKey(string key)
        {
            return _networksList.ContainsKey(key);
        }
    }
}
