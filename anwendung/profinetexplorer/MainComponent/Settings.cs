﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Profinet;

namespace ProfinetExplorer.MainComponent
{
    class SerializedSettings
    {
        private static T DeserializeObject<T>(string toDeserialize)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            System.IO.StringReader textReader = new System.IO.StringReader(toDeserialize);
            return (T)xmlSerializer.Deserialize(textReader);
        }

        private static string SerializeObject<T>(T toSerialize)
        {
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            System.IO.StringWriter textWriter = new System.IO.StringWriter();
            xmlSerializer.Serialize(textWriter, toSerialize);
            return textWriter.ToString();
        }

        public static List<Dcp.BlockOptionMeta> GetAppManufacturerSpecificDcpOptionList()
        {
            List<Dcp.BlockOptionMeta> ret = new List<Dcp.BlockOptionMeta>();
            if (Properties.Settings.Default.ManufacturerSpecificDCPOptions == null) return ret;
            foreach (string str in Properties.Settings.Default.ManufacturerSpecificDCPOptions)
            {
                try
                {
                    ret.Add(DeserializeObject<Dcp.BlockOptionMeta>(str));
                }
                catch (Exception)
                {
                    Utilities.EventLogging.logEvent(null, "Manufacture option invalid: \"" + str + "\"", false, "error");
                }
            }
            return ret;
        }

        public static void AddToAppManufacturerSpecificDcpOptionList(Dcp.BlockOptionMeta option)
        {
            if (Properties.Settings.Default.ManufacturerSpecificDCPOptions == null) Properties.Settings.Default.ManufacturerSpecificDCPOptions = new System.Collections.Specialized.StringCollection();
            string str = SerializeObject<Dcp.BlockOptionMeta>(option);
            Properties.Settings.Default.ManufacturerSpecificDCPOptions.Add(str);
        }
    }
}
